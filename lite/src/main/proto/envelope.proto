syntax = "proto3";

package record.pb;

import "crypto.proto";

// The Envelope message is used in the /lite/push/1.0.0 and /lite/pull/1.0.0 protocol

// Envelope encloses a signed payload produced by a peer, along with the public
// key of the keypair it was signed with so that it can be stateless validated
// by the receiver.
//
// The payload is prefixed with a byte string that determines the type, so it
// can be deserialized deterministically. Often, this byte string is a
// multicodec.
message Envelope {
  // public_key is the public key of the keypair the enclosed payload was
  // signed with.
  crypto.pb.PublicKey public_key = 1;

  // payload_type encodes the type of payload, so that it can be deserialized
  // deterministically.
  bytes payload_type = 2;

  // payload is the actual payload carried inside this envelope.
  // Limited in Lite: cidV1 multihash (SHA2-256,ID), multicodec (DAG-PB, RAW, LIBP2P_KEY)
  bytes payload = 3;

  // signature is the signature produced by the private key corresponding to
  // the enclosed public key, over the payload, prefixing a domain string for
  // additional security.
  bytes signature = 5;
}

// only in Lite
message Request {
    // payload_type encodes the type of payload
    bytes payload_type = 1;
}