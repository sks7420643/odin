package tech.lp2p.core;

import java.util.Objects;

public record Protocol(String name, ALPN alpn, int codec) {

    public static final Protocol MULTISTREAM_PROTOCOL = new Protocol("/multistream/1.0.0", ALPN.libp2p, 1);
    public static final Protocol DHT_PROTOCOL = new Protocol("/ipfs/kad/1.0.0", ALPN.libp2p, 2);
    public static final Protocol IDENTITY_PROTOCOL = new Protocol("/ipfs/id/1.0.0", ALPN.libp2p, 3);
    public static final Protocol IDENTITY_PUSH_PROTOCOL = new Protocol("/ipfs/id/push/1.0.0", ALPN.libp2p, 4);
    public static final Protocol RELAY_PROTOCOL_HOP = new Protocol("/libp2p/circuit/relay/0.2.0/hop", ALPN.libp2p, 5);
    public static final Protocol RELAY_PROTOCOL_STOP = new Protocol("/libp2p/circuit/relay/0.2.0/stop", ALPN.libp2p, 6);

    public static final Protocol LITE_PUSH_PROTOCOL = new Protocol("/lite/push/1.0.0", ALPN.lite, 8);
    public static final Protocol LITE_PULL_PROTOCOL = new Protocol("/lite/pull/1.0.0", ALPN.lite, 9);
    public static final Protocol LITE_FETCH_PROTOCOL = new Protocol("/lite/fetch/1.0.0", ALPN.lite, 10);

    public static Protocol name(ALPN alpn, String protocol) throws Exception {
        if (Objects.requireNonNull(alpn) == ALPN.libp2p) {
            return switch (protocol) {
                case "/multistream/1.0.0" -> MULTISTREAM_PROTOCOL;
                case "/ipfs/kad/1.0.0" -> DHT_PROTOCOL;
                case "/ipfs/id/1.0.0" -> IDENTITY_PROTOCOL;
                case "/ipfs/id/push/1.0.0" -> IDENTITY_PUSH_PROTOCOL;
                case "/libp2p/circuit/relay/0.2.0/hop" -> RELAY_PROTOCOL_HOP;
                case "/libp2p/circuit/relay/0.2.0/stop" -> RELAY_PROTOCOL_STOP;
                default -> throw new Exception("unhandled protocol " + protocol);
            };
        }
        throw new Exception("unhandled protocol " + protocol);

    }

    public static Protocol codec(ALPN alpn, int protocol) throws Exception {
        if (Objects.requireNonNull(alpn) == ALPN.lite) {
            return switch (protocol) {
                case 8 -> LITE_PUSH_PROTOCOL;
                case 9 -> LITE_PULL_PROTOCOL;
                case 10 -> LITE_FETCH_PROTOCOL;
                default -> throw new Exception("unhandled protocol " + protocol);
            };
        }
        throw new Exception("unhandled protocol " + protocol);
    }
}
