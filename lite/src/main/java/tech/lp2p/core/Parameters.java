package tech.lp2p.core;

public record Parameters(ALPN alpn, boolean keepAlive, int initialMaxData) {

    public static Parameters create(ALPN alpn, boolean keepAlive) {
        return new Parameters(alpn, keepAlive, tech.lp2p.quic.Settings.INITIAL_MAX_DATA);
    }

    public static Parameters create(ALPN alpn) {
        return new Parameters(alpn, false, tech.lp2p.quic.Settings.INITIAL_MAX_DATA);
    }

}
