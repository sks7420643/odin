package tech.lp2p.core;


import javax.annotation.Nullable;

public sealed interface Info permits Raw, Fid, Dir {
    long size();

    Cid cid();


    /**
     * @noinspection SameReturnValue
     */
    @Nullable
    String name(); // in case of Raw it will return null

    default boolean isDir() {
        return this instanceof Dir;
    }

    default boolean isRaw() {
        return this instanceof Raw;
    }

    default boolean isFid() {
        return this instanceof Fid;
    }
}
