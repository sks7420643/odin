package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.util.Arrays;

public record Identify(@NonNull PeerId peerId,
                       @NonNull String agent,
                       @NonNull Peeraddr[] peeraddrs,
                       @NonNull String[] protocols) {

    public boolean hasProtocol(String protocol) {
        return Arrays.asList(protocols).contains(protocol);
    }

    public boolean hasRelayHop() {
        return hasProtocol(Protocol.RELAY_PROTOCOL_HOP.name());
    }

}
