package tech.lp2p.core;


public interface Cancellable {
    long MAX_TIMEOUT = 60 * 60; // 1 hour = 3600 sec

    boolean isCancelled();

    default long timeout() {
        return MAX_TIMEOUT;
    }
}
