package tech.lp2p.core;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;

import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;


public record Responder(@NonNull Protocols protocols) {
    private static final String TAG = Responder.class.getSimpleName();

    @Nullable
    private Handler protocolHandler(@NonNull Protocol protocol) {
        return protocols.get(protocol);
    }

    public void protocol(Stream stream, Protocol protocol) throws Exception {
        Handler handler = protocolHandler(protocol);
        if (handler != null) {
            handler.protocol(stream);
        } else {
            Utils.error(TAG, "Ignore " + protocol + " " + stream.connection().remotePeerId());
            stream.writeOutput(Utils.na(), false); // TODO [Future low] maybe closing ?
        }
    }


    public void data(Stream stream, Protocol protocol, byte[] data) throws Exception {
        Objects.requireNonNull(protocol, "data unknown protocol "
                + stream.connection().remotePeerId());
        Handler handler = protocolHandler(protocol);
        if (handler != null) {
            handler.data(stream, data);
        } else {
            throw new Exception("unhandled protocol");
        }

    }


    public void fin(Stream stream, Protocol protocol) {
        Objects.requireNonNull(protocol, "data unknown protocol "
                + stream.connection().remotePeerId());
        Handler handler = protocolHandler(protocol);
        if (handler != null) {
            handler.fin(stream);
        } else {
            Utils.error(TAG, "fin unknown protocol " + stream.connection().remotePeerId());
        }

    }

    public boolean validAlpns(ALPN alpn) {
        for (Protocol protocol : protocols.keySet()) {
            if (protocol.alpn() != alpn) {
                return false;
            }
        }
        return true;
    }
}


