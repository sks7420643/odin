package tech.lp2p.core;

import tech.lp2p.Lite;

public record PrivateKey(byte[] raw) implements java.security.PrivateKey {
    @Override
    public String getAlgorithm() {
        return Lite.Ed25519;
    }

    @Override
    public String getFormat() {
        return null;
    }

    @Override
    public byte[] getEncoded() {
        return raw;
    }
}
