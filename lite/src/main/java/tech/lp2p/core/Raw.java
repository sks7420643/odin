package tech.lp2p.core;

import androidx.annotation.Nullable;

public record Raw(Cid cid, long size) implements Info {

    @Nullable
    @Override
    public String name() {
        return null;
    }
}
