package tech.lp2p.core;

public interface Connection {
    ALPN alpn();

    void close();

    Peeraddr remotePeeraddr();

    PeerId remotePeerId();

    boolean isConnected();

    Host host();
}
