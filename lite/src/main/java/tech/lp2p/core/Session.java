package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.io.Closeable;
import java.io.IOException;


public interface Session extends Closeable, Routing, Host {

    // returns the block store where all data is stored
    @NonNull
    BlockStore blockStore();

    void close() throws IOException;

    @NonNull
    Certificate certificate();
}

