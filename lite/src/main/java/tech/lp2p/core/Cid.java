package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Objects;

import tech.lp2p.utils.Utils;

public record Cid(byte[] multihash, int codec) implements Serializable {

    @NonNull
    public static Cid generateCid(Multicodec ipld, byte[] data) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(data);
        byte[] multihash = Multihash.create(Multihash.SHA2_256, hash);
        return createCid(ipld, multihash);
    }

    @NonNull
    public static Cid createCid(Multicodec ipld, byte[] multihash) {
        return new Cid(multihash, ipld.codec());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cid cid = (Cid) o;
        return codec == cid.codec && Arrays.equals(multihash, cid.multihash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(multihash), codec); // ok, checked, maybe opt
    }

    @NonNull
    @Override
    public String toString() {
        return Multibase.encode(Multibase.BASE32, encoded());
    }

    @NonNull
    public Multicodec getCodec() {
        return Multicodec.get(codec);
    }

    @NonNull
    public byte[] encoded() {

        int versionLength = Utils.unsignedVariantSize(1);
        int codecLength = Utils.unsignedVariantSize(codec);

        ByteBuffer buffer = ByteBuffer.allocate(
                versionLength + codecLength + multihash.length);
        Utils.writeUnsignedVariant(buffer, 1);
        Utils.writeUnsignedVariant(buffer, codec);
        buffer.put(multihash);
        return buffer.array();

    }
}