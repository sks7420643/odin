package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.util.List;

public interface PeerStore {

    List<Peeraddr> randomPeeraddrs(int limit);

    void storePeeraddr(@NonNull Peeraddr peeraddr);

    void removePeeraddr(@NonNull Peeraddr peeraddr);
}
