package tech.lp2p.core;

import androidx.annotation.NonNull;

import java.security.MessageDigest;
import java.util.Arrays;

public record Key(byte[] id, byte[] target) {

    @NonNull
    public static Key convertKey(@NonNull byte[] target) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        return new Key(digest.digest(target), target);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return Arrays.equals(id, key.id);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(id); // ok, checked, maybe opt
    }

}
