package tech.lp2p.dht;

import androidx.annotation.NonNull;

import java.math.BigInteger;
import java.util.concurrent.atomic.AtomicReference;

import tech.lp2p.core.Key;


public record DhtQueryPeer(@NonNull DhtPeer dhtPeer, @NonNull BigInteger distance,
                           @NonNull AtomicReference<DhtPeerState> state)
        implements Comparable<DhtQueryPeer> {


    public static DhtQueryPeer create(@NonNull DhtPeer dhtPeer, @NonNull Key key) {
        BigInteger distance = DhtQueryPeer.distance(key, dhtPeer.key());
        return new DhtQueryPeer(dhtPeer, distance, new AtomicReference<>(DhtPeerState.PeerHeard));
    }

    private static byte[] xor(byte[] x1, byte[] x2) {
        byte[] out = new byte[x1.length];

        for (int i = 0; i < x1.length; i++) {
            out[i] = (byte) (0xff & ((int) x1[i]) ^ ((int) x2[i]));
        }
        return out;
    }

    @NonNull
    public static BigInteger distance(@NonNull Key a, @NonNull Key b) {
        byte[] k3 = xor(a.id(), b.id());

        // SetBytes interprets buf as the bytes of a big-endian unsigned
        // integer, sets z to that value, and returns z.
        // big.NewInt(0).SetBytes(k3)

        return new BigInteger(k3);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DhtQueryPeer dhtQueryPeer = (DhtQueryPeer) o;
        return dhtPeer.equals(dhtQueryPeer.dhtPeer) && distance.equals(dhtQueryPeer.distance);
    }

    @NonNull
    public DhtPeerState getState() {
        return state.get();
    }

    public void setState(@NonNull DhtPeerState state) {
        this.state.set(state);
    }


    @Override
    public int compareTo(DhtQueryPeer o) {
        return distance.compareTo(o.distance);
    }


}
