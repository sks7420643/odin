package tech.lp2p.dht;


import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import dht.pb.Dht;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Server;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.quic.TransportError;
import tech.lp2p.utils.Utils;

public interface DhtService {

    @NonNull
    static Dht.Message createFindNodeMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    static Dht.Message createGetProvidersMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    @NonNull
    static Dht.Message createAddProviderMessage(Server server, Key key) {

        Dht.Message.Builder builder = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.ADD_PROVIDER)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0);

        Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                .setId(ByteString.copyFrom(server.self().multihash()));
        for (Peeraddr peeraddr : server.publicPeeraddrs()) {
            peerBuilder.addAddrs(ByteString.copyFrom(peeraddr.encoded()));
        }
        builder.addProviderPeers(peerBuilder.build());

        return builder.build();
    }


    @NonNull
    static Peeraddrs providers(Connection connection, Key key) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("wrong ALPN usage");
        }

        Dht.Message message = DhtService.createGetProvidersMessage(key);

        CompletableFuture<Dht.Message> done = new CompletableFuture<>();

        StreamRequester.createStream((tech.lp2p.quic.Connection) connection, new DhtRequest(done))
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);

        Dht.Message pms = done.get(Lite.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);

        Peeraddrs peeraddrs = new Peeraddrs();
        List<Dht.Message.Peer> list = pms.getProviderPeersList();
        for (Dht.Message.Peer entry : list) {
            PeerId peerId = PeerId.create(entry.getId().toByteArray());
            Peeraddr resolved = Peeraddrs.reduceToOne(peerId, entry.getAddrsList());
            if (resolved != null) {
                peeraddrs.add(resolved);
            }
        }
        return peeraddrs;
    }

    static Peeraddrs findPeer(Connection connection, Key key) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("wrong ALPN usage");
        }

        Dht.Message message = DhtService.createFindNodeMessage(key);

        CompletableFuture<Dht.Message> done = new CompletableFuture<>();

        StreamRequester.createStream((tech.lp2p.quic.Connection) connection, new DhtRequest(done))
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);

        Dht.Message pms = done.get(Lite.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);

        Peeraddrs peeraddrs = new Peeraddrs();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();
        for (Dht.Message.Peer entry : list) {
            PeerId peerId = PeerId.create(entry.getId().toByteArray());
            Peeraddr resolved = Peeraddrs.reduceToOne(peerId, entry.getAddrsList());
            if (resolved != null) {
                peeraddrs.add(resolved);
            }
        }
        return peeraddrs;
    }

    static void message(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ExecutionException {

        CompletableFuture<Void> done = new CompletableFuture<>();

        StreamRequester.createStream((tech.lp2p.quic.Connection) connection, new DhtMessage(done))
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_MESSAGE_TIMEOUT);
        done.get(Lite.DHT_MESSAGE_TIMEOUT, TimeUnit.SECONDS);

    }

    @NonNull
    static Dht.Message request(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ExecutionException {

        CompletableFuture<Dht.Message> done = new CompletableFuture<>();

        StreamRequester.createStream((tech.lp2p.quic.Connection) connection, new DhtRequest(done))
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);

        return done.get(Lite.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);

    }

    record DhtMessage(CompletableFuture<Void> done) implements StreamRequester {
        private static final String TAG = DhtMessage.class.getSimpleName();

        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void protocol(Stream stream, Protocol protocol) throws Exception {
            if (!Arrays.asList(Protocol.MULTISTREAM_PROTOCOL,
                    Protocol.DHT_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
            if (Objects.equals(protocol, Protocol.DHT_PROTOCOL)) {
                done.complete(null); // this is not 100% correct [a fin would be much nicer]
                stream.closeInput(TransportError.Code.NO_ERROR);
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            throw new Exception("data method invoked [not expected]");
        }

        @Override
        public void fin(Stream stream) {
            // this is shit, it seems never invoked (otherwise I can react when message is done)
            Utils.error(TAG, "fin received " + stream.connection().remotePeerId());
        }
    }

    record DhtRequest(CompletableFuture<Dht.Message> done) implements StreamRequester {

        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, Protocol protocol) throws Exception {
            if (!Arrays.asList(Protocol.MULTISTREAM_PROTOCOL,
                    Protocol.DHT_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            done.complete(Dht.Message.parseFrom(data));
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void fin(Stream stream) {
            if (!done.isDone()) {
                throwable(new Throwable("Fin received instead of message"));
            }
        }
    }
}
