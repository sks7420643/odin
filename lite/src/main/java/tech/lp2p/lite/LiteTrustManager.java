package tech.lp2p.lite;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;

import javax.net.ssl.X509TrustManager;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.PeerId;
import tech.lp2p.utils.Utils;

@SuppressLint("CustomX509TrustManager")
public final class LiteTrustManager implements X509TrustManager {
    private static final String TAG = LiteTrustManager.class.getSimpleName();
    @Nullable
    private final PeerId expected;

    public LiteTrustManager(@NonNull PeerId expected) {
        this.expected = expected;
    }

    public LiteTrustManager() {
        this.expected = null;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        try {

            if (chain.length != 1) {
                throw new Exception("only one certificate allowed");
            }

            // here the expected peerId is null (because it is simply not known)
            // just check if the extracted peerId is not null
            for (X509Certificate cert : chain) {

                Certificate.validCertificate(cert);

                PeerId peerId = Certificate.extractPeerId(cert);
                Objects.requireNonNull(peerId);
            }
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
            throw new CertificateException(throwable);
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        try {

            if (chain.length != 1) {
                throw new Exception("only one certificate allowed");
            }

            Objects.requireNonNull(expected); // expected is required
            // now check the extracted peer ID with the expected value
            for (X509Certificate cert : chain) {

                Certificate.validCertificate(cert);

                PeerId peerId = Certificate.extractPeerId(cert);
                Objects.requireNonNull(peerId);
                if (!Objects.equals(peerId, expected)) {
                    throw new CertificateException("PeerIds not match " + peerId + " " + expected);
                }
            }
        } catch (Throwable throwable) {
            throw new CertificateException(throwable);
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return Utils.CERTIFICATES_EMPTY;
    }

}
