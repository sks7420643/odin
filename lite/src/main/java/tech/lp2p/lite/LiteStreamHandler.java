package tech.lp2p.lite;

import tech.lp2p.core.Handler;
import tech.lp2p.core.Protocol;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class LiteStreamHandler implements Handler {

    @Override
    public void protocol(Stream stream) {
        if (!stream.isInitiator()) { // TODO [Future high] make it independent of it
            stream.writeOutput(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL), false);
        }
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        throw new Exception("should not be invoked");
    }

    @Override
    public void fin(Stream stream) {
        Utils.error(getClass().getSimpleName(), "fin received" +
                stream.connection().remotePeerId());
    }

}
