package tech.lp2p.lite;


import androidx.annotation.NonNull;

import java.security.KeyPair;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddrs;


public final class LiteHost {
    @NonNull
    private final KeyPair keyPair;
    @NonNull
    private final PeerId self;
    @NonNull
    private final Certificate certificate;
    @NonNull
    private final String agent;
    @NonNull
    private final Peeraddrs bootstrap;
    @NonNull
    private final Payloads payloads;

    public LiteHost(@NonNull KeyPair keyPair,
                    @NonNull Peeraddrs bootstrap,
                    @NonNull String agent,
                    @NonNull Payloads payloads) throws Exception {
        this.keyPair = keyPair;
        this.bootstrap = bootstrap;
        this.agent = agent;
        this.payloads = payloads;
        this.certificate = Certificate.createCertificate(keyPair);
        this.self = Keys.createPeerId(keyPair.getPublic());
    }

    @NonNull
    public Payloads payloads() {
        return payloads;
    }

    @NonNull
    public KeyPair keyPair() {
        return keyPair;
    }

    @NonNull
    public PeerId self() {
        return self;
    }

    @NonNull
    public Peeraddrs bootstrap() {
        return bootstrap;
    }

    @NonNull
    public Certificate certificate() {
        return certificate;
    }


    @NonNull
    public String agent() {
        return agent;
    }

}


