package tech.lp2p.lite;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Objects;

import merkledag.pb.Merkledag;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.dag.DagReader;
import tech.lp2p.dag.DagService;

public record LiteReader(@NonNull DagReader dagReader,
                         @NonNull DagFetch dagFetch,
                         @NonNull Cancellable cancellable) {

    @NonNull
    public static LiteReader getReader(@NonNull Connection connection, @NonNull Cid cid,
                                       @NonNull Cancellable cancellable)
            throws Exception {
        LiteFetchManager fetchManager = new LiteFetchManager(connection);
        return LiteReader.createReader(cancellable, fetchManager, cid);
    }

    @NonNull
    public static LiteReader getReader(@NonNull Session session, @NonNull Cid cid) throws Exception {
        return LiteReader.createReader(() -> false, new LiteLocalFetch(session), cid);
    }

    public static LiteReader createReader(@NonNull Cancellable cancellable,
                                          @NonNull DagFetch dagFetch, @NonNull Cid cid)
            throws Exception {

        Merkledag.PBNode top = DagService.getNode(dagFetch, cancellable, cid);
        Objects.requireNonNull(top);
        DagReader dagReader = DagReader.create(top);

        return new LiteReader(dagReader, dagFetch, cancellable);
    }

    @NonNull
    public ByteString loadNextData() throws Exception {
        return dagReader.loadNextData(dagFetch, cancellable);
    }

    public void seek(int position) throws Exception {
        dagReader.seek(dagFetch, cancellable, position);
    }

    public long getSize() {
        return this.dagReader.size();
    }
}
