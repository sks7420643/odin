package tech.lp2p.lite;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.KeyPair;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cancellable;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Key;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Reservations;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Server;
import tech.lp2p.dht.DhtHandler;
import tech.lp2p.dht.DhtKademlia;
import tech.lp2p.ident.IdentifyHandler;
import tech.lp2p.ident.IdentifyPushHandler;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.AlpnLibp2pResponder;
import tech.lp2p.quic.AlpnLiteResponder;
import tech.lp2p.quic.ApplicationProtocolConnectionFactory;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.ServerConnection;
import tech.lp2p.quic.ServerConnector;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamData;
import tech.lp2p.quic.TransportError;
import tech.lp2p.quic.Version;
import tech.lp2p.relay.RelayHopHandler;
import tech.lp2p.relay.RelayService;
import tech.lp2p.relay.RelayStopHandler;
import tech.lp2p.utils.Utils;

public final class LiteServer implements Server {
    private static final String TAG = LiteServer.class.getSimpleName();

    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    @NonNull
    private final Map<Connection, Reservation> reservations = new ConcurrentHashMap<>();
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector serverConnector;
    @NonNull
    private final Responder responderLibp2p;
    @NonNull
    private final Responder responderLite;
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Function<Payload, Envelope> envelopeSupplier;
    @NonNull
    private final Consumer<Envelope> envelopeConsumer;
    @NonNull
    private final DhtKademlia dhtKademlia;
    @NonNull
    private final BlockStore blockStore;

    private LiteServer(@NonNull Server.Settings settings,
                       @NonNull LiteHost host,
                       @NonNull BlockStore blockStore,
                       @NonNull PeerStore peerStore,
                       @NonNull Function<Payload, Envelope> envelopeSupplier,
                       @NonNull Consumer<Envelope> envelopeConsumer,
                       @NonNull DatagramSocket socket) {
        this.host = host;
        this.blockStore = blockStore;
        this.socket = socket;
        this.envelopeSupplier = envelopeSupplier;
        this.envelopeConsumer = envelopeConsumer;
        this.serverConnector = createServerConnector(socket);
        this.dhtKademlia = new DhtKademlia(this, peerStore);

        Limit limit = settings.limit();
        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        libp2p.put(Protocol.IDENTITY_PUSH_PROTOCOL, new IdentifyPushHandler());
        libp2p.put(Protocol.DHT_PROTOCOL, new DhtHandler(dhtKademlia));
        libp2p.put(Protocol.RELAY_PROTOCOL_HOP, new RelayHopHandler(this, limit));
        this.responderLibp2p = new Responder(libp2p);

        Protocols lite = new Protocols();
        lite.put(Protocol.LITE_PUSH_PROTOCOL, new LitePushHandler(this));
        lite.put(Protocol.LITE_PULL_PROTOCOL, new LitePullHandler(this));
        lite.put(Protocol.LITE_FETCH_PROTOCOL, new LiteFetchHandler(blockStore));
        this.responderLite = new Responder(lite);


    }

    private static int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    @NonNull
    public static Server startServer(@NonNull Server.Settings settings,
                                     @NonNull LiteHost host,
                                     @NonNull BlockStore blockStore,
                                     @NonNull PeerStore peerStore,
                                     @NonNull Consumer<Peeraddr> connectionGain,
                                     @NonNull Consumer<Peeraddr> connectionLost,
                                     @NonNull Consumer<Reservation> reservationGain, // todo
                                     @NonNull Consumer<Reservation> reservationLost, // todo
                                     @NonNull Function<PeerId, Boolean> isGated,
                                     @NonNull Function<Payload, Envelope> envelopeSupplier,
                                     @NonNull Consumer<Envelope> envelopeConsumer) {

        DatagramSocket socket = Utils.getSocket(settings.port());

        LiteServer server = new LiteServer(settings, host, blockStore, peerStore,
                envelopeSupplier, envelopeConsumer, socket);

        ServerConnector serverConnector = server.serverConnector;

        serverConnector.setClosedConsumer(connectionLost);
        ApplicationProtocolConnection protocolConnection =
                new ApplicationProtocolConnection(isGated, connectionGain);

        serverConnector.registerApplicationProtocol(ALPN.libp2p.name(), protocolConnection);

        serverConnector.registerApplicationProtocol(ALPN.lite.name(), protocolConnection);

        serverConnector.start();
        return server;
    }

    @NonNull
    public ServerConnector serverConnector() {
        return serverConnector;
    }

    void push(@NonNull Envelope envelope) {
        // TODO execute in a virtual thread
        envelopeConsumer.accept(envelope);
    }

    @Override
    @NonNull
    public Responder responder(@NonNull ALPN alpn) {
        switch (alpn) {
            case lite -> {
                return responderLite;
            }
            case libp2p -> {
                return responderLibp2p;
            }
        }
        throw new IllegalStateException();
    }

    @NonNull
    @Override
    public KeyPair keyPair() {
        return host.keyPair();
    }

    @NonNull
    private ServerConnector createServerConnector(DatagramSocket socket) {
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.QUIC_version_1);

        Map<ALPN, Function<Stream, Consumer<StreamData>>> streamDataConsumers =
                Map.of(ALPN.libp2p,
                        quicStream -> AlpnLibp2pResponder.create(responderLibp2p),
                        ALPN.lite,
                        quicStream -> AlpnLiteResponder.create(responderLite));

        return new ServerConnector(this, socket, new LiteTrustManager(),
                host.certificate().certificate(),
                host.certificate().privateKey(), supportedVersions, streamDataConsumers);

    }

    @Nullable
    Envelope envelope(@NonNull Payload payload) {
        return envelopeSupplier.apply(payload);
    }

    @NonNull
    public DatagramSocket socket() {
        return socket;
    }

    @Override
    public void shutdown() {
        serverConnector.shutdown();
    }

    @Override
    public int port() {
        return socket.getLocalPort();
    }

    @Override
    public int numConnections() {
        return serverConnector.numConnections();
    }

    private void punching(Peeraddr peeraddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = peeraddr.inetAddress();
            int port = peeraddr.port();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            socket.send(datagram);
            Utils.debug(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(peeraddr);
            }

            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    public void holePunching(@NonNull Peeraddr peeraddr) {
        try {
            ExecutorService service = Executors.newSingleThreadExecutor();
            service.execute(() -> punching(peeraddr));
            boolean finished = service.awaitTermination(Lite.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    @Override
    public PeerId self() {
        return host.self();
    }

    @NonNull
    @Override
    public Peeraddrs bootstrap() {
        return host.bootstrap();
    }

    @NonNull
    @Override
    public Protocols protocols(@NonNull ALPN alpn) {
        return responder(alpn).protocols();
    }

    @NonNull
    @Override
    public Certificate certificate() {
        return host.certificate();
    }

    @NonNull
    public Reservations reservations() {
        Reservations result = new Reservations();
        for (Map.Entry<Connection, Reservation> entry : reservations.entrySet()) {
            try {
                if (entry.getKey().isConnected()) {
                    result.add(entry.getValue());
                } else {
                    reservations.remove(entry.getKey());
                }
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable);
            }
        }
        return result;
    }


    @NonNull
    private Set<Reservation> reservations(Collection<Peeraddr> peeraddrs, Cancellable cancellable) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = new HashSet<>();
            Set<Connection> updateRequired = new HashSet<>();
            // Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Map.Entry<Connection, Reservation> entry : reservations.entrySet()) {

                // check if still a connection
                // only for safety here
                if (!entry.getKey().isConnected()) {
                    continue;
                }

                if (entry.getValue().limit().limited()) {
                    if (entry.getValue().expireInMinutes() > 2) {
                        relaysStillValid.add(entry.getValue().peerId());
                    } else {
                        // update the connection
                        updateRequired.add(entry.getKey());
                    }
                } else { // static relay
                    // still valid
                    relaysStillValid.add(entry.getValue().peerId());
                }
            }

            ExecutorService service = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            boolean workToDo = !updateRequired.isEmpty() || !peeraddrs.isEmpty();
            for (Connection connection : updateRequired) {
                service.execute(() -> {
                    try {
                        if (cancellable.isCancelled()) {
                            return;
                        }
                        Reservation reservation =
                                RelayService.reserveHopRequest(self(), connection);
                        reservations.put(connection, reservation);
                    } catch (Throwable throwable) {
                        Utils.error(TAG, throwable.getClass().getSimpleName() + " "
                                + throwable.getMessage());
                    }
                });

            }

            for (Peeraddr address : peeraddrs) {
                service.execute(() -> {
                    try {
                        if (cancellable.isCancelled()) {
                            return;
                        }
                        PeerId peerId = address.peerId();
                        Objects.requireNonNull(peerId);

                        if (!relaysStillValid.contains(peerId)) {
                            reservation(address);
                        }

                    } catch (Throwable throwable) {
                        Utils.error(TAG, throwable.getClass().getSimpleName() + " "
                                + throwable.getMessage());
                    }
                });

            }

            if (workToDo) {
                service.shutdown();
                if (cancellable.timeout() > 0) {
                    try {
                        boolean termination = service.awaitTermination(
                                cancellable.timeout(), TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }

        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    @VisibleForTesting
    @Override
    @NonNull
    public Reservation reservation(Peeraddr peeraddr) throws Exception {


        Parameters parameters = Parameters.create(ALPN.libp2p, true);

        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        libp2p.put(Protocol.IDENTITY_PUSH_PROTOCOL, new IdentifyPushHandler());
        libp2p.put(Protocol.RELAY_PROTOCOL_STOP, new RelayStopHandler(this));

        Connection connection = ConnectionBuilder.connect(this, peeraddr,
                parameters, this.certificate(), new Responder(libp2p));
        try {
            Identify identify = IdentifyService.identify(connection);
            if (!identify.hasRelayHop()) {
                throw new Exception("No relay hop protocol [abort]");
            }

            Reservation reservation = RelayService.reserveHopRequest(this.self(), connection);
            reservations.put(connection, reservation);
            return reservation;
        } catch (Throwable throwable) {
            connection.close();
            throw throwable;
        }
    }

    public boolean hasReservations() {
        return !reservations.isEmpty();
    }

    public void reservations(Cancellable cancellable) {

        Set<Reservation> reservations = reservations(Collections.emptyList(), cancellable);

        Set<Peeraddr> relays = ConcurrentHashMap.newKeySet();
        for (Reservation reservation : reservations) {
            relays.add(reservation.peeraddr());
        }

        if (cancellable.isCancelled()) {
            return;
        }


        // fill up reservations [not yet enough]
        dhtKademlia.findClosestPeers(cancellable, multiaddr -> {

            if (relays.contains(multiaddr)) {
                return;
            }

            if (cancellable.isCancelled()) {
                return;
            }

            try {
                reservation(multiaddr);
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable.getClass().getSimpleName());
            }
        }, self());

    }

    @NonNull
    public Peeraddrs publicPeeraddrs() {
        try {
            return Peeraddr.publicPeeraddrs(self(), port());
        } catch (Throwable throwable) {
            Utils.error(TAG, throwable);
            return new Peeraddrs();
        }
    }

    @NonNull
    public Identify identify() throws Exception {
        return IdentifyService.identify(IdentifyService.identify(
                host.keyPair(), host.agent(),
                protocols(ALPN.libp2p).keySet(), publicPeeraddrs()), self());

    }

    @NonNull
    public Peeraddrs reservationPeeraddrs() {
        Peeraddrs result = new Peeraddrs();
        for (Reservation reservation : reservations()) {
            result.add(reservation.peeraddr());
        }
        return result;
    }

    @Override
    public boolean hasPublicPeeraddrs() {
        return !publicPeeraddrs().isEmpty();
    }

    public void provideKey(@NonNull Cancellable cancellable, @NonNull Consumer<Peeraddr> consumer,
                           @NonNull Key key) {
        dhtKademlia.provideKey(cancellable, this, consumer, key);
    }

    @Override
    public Reservation updateReservation(Reservation reservation) throws Exception {
        for (Map.Entry<Connection, Reservation> entry : reservations.entrySet()) {

            if (Objects.equals(entry.getValue(), reservation)) {
                Connection connection = entry.getKey();
                if (!connection.isConnected()) {
                    throw new Exception("Reservation not valid anymore");
                } else {
                    Reservation updated =
                            RelayService.reserveHopRequest(self(), connection);
                    reservations.put(connection, reservation);
                    return updated;
                }
            }
        }
        throw new Exception("Reservation not valid anymore");
    }

    @NonNull
    public String agent() {
        return host.agent();
    }

    @NonNull
    @Override
    public Payloads payloads() {
        return host.payloads();
    }

    @NonNull
    @Override
    public BlockStore blockStore() {
        return blockStore;
    }

    private static class ApplicationProtocolConnection extends ApplicationProtocolConnectionFactory {
        private final Function<PeerId, Boolean> isGated;
        private final Consumer<Peeraddr> connectConsumer;

        public ApplicationProtocolConnection(
                Function<PeerId, Boolean> isGated, Consumer<Peeraddr> connectConsumer) {
            this.isGated = isGated;
            this.connectConsumer = connectConsumer;
        }

        @Override
        public void createConnection(String protocol, ServerConnection connection) throws TransportError {
            PeerId remotePeerId;

            try {
                X509Certificate certificate = connection.remoteCertificate();
                Objects.requireNonNull(certificate);
                remotePeerId = Certificate.extractPeerId(certificate);
                Objects.requireNonNull(remotePeerId);
            } catch (Throwable throwable) {
                throw new TransportError(TransportError.Code.APPLICATION_ERROR,
                        throwable.getMessage());
            }

            // now the remote PeerId is available
            connection.setRemotePeerId(remotePeerId);

            if (isGated.apply(remotePeerId)) {
                throw new TransportError(TransportError.Code.APPLICATION_ERROR,
                        "Peer is gated " + remotePeerId);
            }

            try {
                connectConsumer.accept(connection.remotePeeraddr());
            } catch (Throwable throwable) {
                Utils.error(TAG, throwable);
            }

        }
    }
}
