package tech.lp2p.relay;

import static circuit.pb.Circuit.Status.CONNECTION_FAILED;
import static circuit.pb.Circuit.Status.MALFORMED_MESSAGE;
import static circuit.pb.Circuit.Status.PERMISSION_DENIED;
import static circuit.pb.Circuit.Status.UNEXPECTED_MESSAGE;
import static circuit.pb.Circuit.Status.UNRECOGNIZED;
import static tech.lp2p.quic.StreamRequester.RELAYED;
import static tech.lp2p.quic.StreamRequester.STREAM;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import tech.lp2p.Lite;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Limit;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Protocol;
import tech.lp2p.lite.LiteServer;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.quic.TransportError;
import tech.lp2p.utils.Utils;

public record RelayHopHandler(LiteServer server, Limit limit) implements Handler {

    public static void createStatusMessage(Stream stream, Circuit.Status status) {
        Circuit.HopMessage.Builder builder =
                Circuit.HopMessage.newBuilder()
                        .setType(Circuit.HopMessage.Type.STATUS);
        builder.setStatus(status);
        stream.writeOutput(Utils.encode(builder.build()), true);
    }


    @NonNull
    RelayStopRequest stopRequest(Connection connection, PeerId target) throws Exception {

        Circuit.Peer peer = Circuit.Peer.newBuilder()
                .setId(ByteString.copyFrom(target.multihash()))
                .build();

        CompletableFuture<Circuit.StopMessage> done = new CompletableFuture<>();

        Circuit.StopMessage message = Circuit.StopMessage.newBuilder()
                .setType(Circuit.StopMessage.Type.CONNECT)
                .setPeer(peer)
                .build();

        Stream stream = StreamRequester.createStream(connection, new StopRequest(done, limit));
        stream.writeOutput(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                Protocol.RELAY_PROTOCOL_STOP), false);

        return new RelayStopRequest(stream,
                done.get(Lite.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS));
    }

    private Circuit.Reservation createReservation() {
        Circuit.Reservation.Builder builder = Circuit.Reservation.newBuilder();
        builder.setExpire(createExpire());
        return builder.build();

    }

    private long createExpire() {
        return (System.currentTimeMillis() * 1000) + limit.duration();
    }

    private Circuit.Limit createLimit() {
        return Circuit.Limit.newBuilder()
                .setData(limit.data())
                .setDuration(limit.duration())
                .build();
    }

    private void handleReserveMessage(Stream stream) {

        try {
            PeerId peerId = stream.connection().remotePeerId();
            if (Objects.equals(peerId, server.self())) {
                createStatusMessage(stream, PERMISSION_DENIED);
                return;
            }
            Connection connection = getRelayedConnection(peerId);

            if (connection != null) {
                // remote user has already a reserved connection
                // case 1: the connections are different -> refused connection
                // case 2: just update the connection with new limit
                if (connection != stream.connection()) {
                    createStatusMessage(stream, Circuit.Status.RESERVATION_REFUSED);
                    return;
                }
            }

            Circuit.Reservation reservation = createReservation();
            Circuit.HopMessage.Builder builder = Circuit.HopMessage.newBuilder()
                    .setType(Circuit.HopMessage.Type.STATUS);
            builder.setReservation(reservation);
            builder.setLimit(createLimit());
            builder.setStatus(Circuit.Status.OK);

            stream.connection().setAttribute(RELAYED, new Relayed());

            stream.writeOutput(Utils.encode(builder.build()), true);

        } catch (Throwable throwable) {
            createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
        }
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(Utils.encodeProtocols(Protocol.RELAY_PROTOCOL_HOP), false);
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        if (stream.hasAttribute(STREAM)) {
            Relayed relayed = (Relayed) stream.connection()
                    .getAttribute(RELAYED);
            Objects.requireNonNull(relayed, "Relayed is not defined");
            relayed.incrementBytes(data.length);
            if (relayed.isExpired(limit) || relayed.isDataLimitReached(limit)) {
                RelayService.closeConnection(stream, "Limit is reached");
                return;
            }

            try {
                Stream target = (Stream) stream.getAttribute(STREAM);
                Objects.requireNonNull(target);
                target.writeOutput(Utils.encode(data), false);
            } catch (Throwable throwable) {
                RelayService.closeConnection(stream, throwable.getMessage());
            }
        } else {
            try {
                Circuit.HopMessage hopMessage = Circuit.HopMessage.parseFrom(data);
                Objects.requireNonNull(hopMessage);

                switch (hopMessage.getType()) {
                    case RESERVE -> handleReserveMessage(stream);
                    case CONNECT -> handleConnectMessage(stream, hopMessage);
                    case UNRECOGNIZED -> createStatusMessage(stream, UNRECOGNIZED);
                    case STATUS -> createStatusMessage(stream, UNEXPECTED_MESSAGE);
                }
            } catch (Throwable throwable) {
                createStatusMessage(stream, UNEXPECTED_MESSAGE);
            }
        }
    }

    @Nullable
    private Connection getRelayedConnection(PeerId peerId) {
        Collection<Connection> connections = server.serverConnector().getConnections(peerId);
        for (Connection connection : connections) {
            if (connection.hasAttribute(RELAYED)) {
                Relayed relayed = (Relayed) connection.getAttribute(RELAYED);
                Objects.requireNonNull(relayed);
                if (!relayed.isExpired(limit)) {
                    return connection;
                } else {
                    connection.close(new TransportError(
                            TransportError.Code.APPLICATION_ERROR,
                            "Reservation is expired"));
                }
            }
        }
        return null;
    }

    private void makeStopRequest(Stream stream, Connection connection, PeerId peerId) {
        try {
            RelayStopRequest request = stopRequest(connection, peerId);
            Circuit.StopMessage message = request.message();
            switch (message.getType()) {
                case STATUS -> {
                    Circuit.HopMessage.Builder builder =
                            Circuit.HopMessage.newBuilder()
                                    .setType(Circuit.HopMessage.Type.STATUS);
                    builder.setStatus(message.getStatus());

                    boolean isFinal = message.getStatus() != Circuit.Status.OK;
                    if (!isFinal) {
                        builder.setLimit(createLimit());
                        stream.connection().setAttribute(RELAYED, new Relayed());
                        stream.setAttribute(STREAM, request.stream());
                        request.stream().setAttribute(STREAM, stream);
                    }
                    stream.writeOutput(Utils.encode(builder.build()), isFinal);
                }
                case CONNECT -> createStatusMessage(stream, UNEXPECTED_MESSAGE);
                case UNRECOGNIZED -> createStatusMessage(stream, UNRECOGNIZED);
            }
        } catch (Throwable throwable) {
            createStatusMessage(stream, UNEXPECTED_MESSAGE);
        }
    }

    private void handleConnectMessage(Stream stream, Circuit.HopMessage hopMessage) {

        PeerId remotePeerId = stream.connection().remotePeerId();
        if (Objects.equals(remotePeerId, server.self())) {
            createStatusMessage(stream, PERMISSION_DENIED);
            return;
        }

        if (getRelayedConnection(remotePeerId) != null) {
            // check if the user has an connect request
            createStatusMessage(stream, PERMISSION_DENIED);
            return;
        }

        if (!hopMessage.hasPeer()) {
            createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
            return;
        }

        PeerId peerId = PeerId.create(hopMessage.getPeer().getId().toByteArray());

        if (Objects.equals(peerId, server.self())) {
            createStatusMessage(stream, PERMISSION_DENIED);
            return;
        }
        Connection connection = getRelayedConnection(peerId);

        if (connection != null) {
            Executors.newSingleThreadExecutor().execute(() ->
                    makeStopRequest(stream, connection, peerId));
        } else {
            createStatusMessage(stream, CONNECTION_FAILED);
        }
    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        Utils.error(getClass().getSimpleName(), "fin received " +
                stream.connection().remotePeerId()); // todo maybe notify the relayed stream (check)

    }

    private static class Relayed {
        private final long timestamp;
        private long bytes = 0;

        public Relayed() {
            this.timestamp = System.currentTimeMillis();
        }

        public boolean isExpired(Limit limit) {
            if (limit.duration() == 0) {
                return false;
            }
            return System.currentTimeMillis() > (timestamp + (limit.duration() * 1000L));
        }

        public void incrementBytes(int length) {
            bytes = bytes + length;
        }

        public boolean isDataLimitReached(Limit limit) {
            if (limit.data() == 0) {
                return false;
            }
            return bytes >= limit.data();
        }
    }

    record StopRequest(CompletableFuture<Circuit.StopMessage> done,
                       Limit limit) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void protocol(Stream stream, Protocol protocol) throws Exception {

            if (!Arrays.asList(Protocol.MULTISTREAM_PROTOCOL,
                    Protocol.RELAY_PROTOCOL_STOP).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void fin(Stream stream) {
            Utils.error(getClass().getSimpleName(), "fin received " +
                    stream.connection().remotePeerId());  // todo  maybe notify the relayed stream (check)
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream finished before data"));
            }
        }

        @Override
        public void data(Stream stream, byte[] data) {
            if (stream.hasAttribute(STREAM)) {
                RelayHopHandler.Relayed relayed = (RelayHopHandler.Relayed) stream.connection()
                        .getAttribute(RELAYED);
                Objects.requireNonNull(relayed, "Relayed is not defined");
                relayed.incrementBytes(data.length);
                if (relayed.isExpired(limit) || relayed.isDataLimitReached(limit)) {
                    RelayService.closeConnection(stream, "Limit is reached");
                    return;
                }
                try {
                    Stream target = (Stream) stream.getAttribute(STREAM);
                    Objects.requireNonNull(target);
                    target.writeOutput(Utils.encode(data), false);
                } catch (Throwable throwable) {
                    RelayService.closeConnection(stream, throwable.getMessage());
                }

            } else {
                try {
                    done.complete(Circuit.StopMessage.parseFrom(data));
                } catch (Throwable throwable) {
                    createStatusMessage(stream, MALFORMED_MESSAGE);
                }
            }
        }
    }

    private record RelayStopRequest(Stream stream, Circuit.StopMessage message) {
    }
}
