package tech.lp2p.relay;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import holepunch.pb.Holepunch;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Session;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.quic.TransportError;
import tech.lp2p.utils.Utils;

public interface RelayService {


    static void closeConnection(Stream stream, String message) {
        stream.stopSendingStream(TransportError.Code.APPLICATION_ERROR.value());
        stream.connection().close(new TransportError(
                TransportError.Code.APPLICATION_ERROR, message));
    }

    @NonNull
    static Connection hopConnect(@NonNull Session session,
                                 @NonNull Peeraddr relayAddress,
                                 @NonNull PeerId target,
                                 @NonNull Parameters parameters) throws Exception {
        Connection connection = ConnectionBuilder.connect(session,
                relayAddress, Parameters.create(ALPN.libp2p),
                session.certificate(), session.responder(ALPN.libp2p));
        try {
            RelayInfo relayInfo = createRelayPunchInfo(session, target, parameters);
            return hopConnect(session, connection, relayInfo);
        } finally {
            connection.close();
        }
    }

    @NonNull
    static RelayInfo createRelayPunchInfo(@NonNull Session session,
                                          @NonNull PeerId target,
                                          @NonNull Parameters parameters)
            throws Exception {
        DatagramSocket socket = new DatagramSocket();

        Peeraddrs peeraddrs = Peeraddr.peeraddrs(session.self(), socket.getLocalPort());
        if (peeraddrs.isEmpty()) {
            throw new ConnectException("Hole punching not possible [abort]");
        }
        return new RelayInfo(target, peeraddrs, socket, parameters);
    }


    @NonNull
    private static Connection hopConnect(@NonNull Session session,
                                         @NonNull Connection connection,
                                         @NonNull RelayInfo relayInfo)
            throws Exception {
        try {
            Identify identify = IdentifyService.identify(connection);

            if (!identify.hasRelayHop()) {
                throw new ConnectException("No relay hop protocol [abort]");
            }

            return connectHopRequest(session, connection, relayInfo)
                    .get(Lite.RELAY_CONNECT_TIMEOUT, TimeUnit.SECONDS);

        } catch (Throwable throwable) {
            relayInfo.socket().close();
            throw throwable;
        }
    }

    static Reservation reserveHopRequest(PeerId self, Connection connection) throws Exception {


        Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                .setType(Circuit.HopMessage.Type.RESERVE).build();

        CompletableFuture<Reservation> done = new CompletableFuture<>();


        StreamRequester.createStream((tech.lp2p.quic.Connection) connection,
                new ReserveRequest(done, self)).request(
                Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.RELAY_PROTOCOL_HOP), Lite.DEFAULT_REQUEST_TIMEOUT);

        return done.get(Lite.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
    }


    static CompletableFuture<Connection> connectHopRequest(@NonNull Session session,
                                                           @NonNull Connection connection,
                                                           @NonNull RelayInfo relayInfo) {


        CompletableFuture<Connection> done = new CompletableFuture<>();

        try {
            Circuit.Peer dest = Circuit.Peer.newBuilder()
                    .setId(ByteString.copyFrom(relayInfo.target().multihash()))
                    .build();

            Circuit.HopMessage message = Circuit.HopMessage.newBuilder()
                    .setType(Circuit.HopMessage.Type.CONNECT)
                    .setPeer(dest)
                    .build();

            StreamRequester.createStream((tech.lp2p.quic.Connection) connection, new ConnectRequest(
                    done, session, relayInfo)).writeOutput(
                    Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                            Protocol.RELAY_PROTOCOL_HOP), false);

        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
        }
        return done;
    }

    static Limit getLimit(Circuit.HopMessage hopMessage) {
        long limitData = 0;
        int limitDuration = 0;

        if (hopMessage.hasLimit()) {
            Circuit.Limit limit = hopMessage.getLimit();
            limitData = limit.getData();
            limitDuration = limit.getDuration();
        }

        return new Limit(limitData, limitDuration);
    }


    record ConnectRequest(CompletableFuture<Connection> done,
                          Session session,
                          RelayInfo relayInfo) implements StreamRequester {


        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
            Utils.error(getClass().getSimpleName(), "fin received " +
                    stream.connection().remotePeerId()); // todo
        }


        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void protocol(Stream stream, Protocol protocol) throws Exception {

            if (!Arrays.asList(Protocol.MULTISTREAM_PROTOCOL,
                    Protocol.RELAY_PROTOCOL_HOP).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {

            if (stream.hasAttribute(INITIALIZED)) {
                try {
                    SyncInfo syncInfo = sendSync(stream, data);

                    // Next, B wait for rtt/2
                    // the creating of a connection should not block, it will otherwise block
                    // all reading of the underlying port
                    Thread.sleep(syncInfo.rtt() / 2);


                    Executors.newSingleThreadExecutor().execute(() -> {
                        try {
                            Parameters parameters = relayInfo.parameters();
                            done.complete(ConnectionBuilder.connect(session,
                                    syncInfo.peeraddr(), parameters,
                                    session.certificate(),
                                    session.responder(parameters.alpn()),
                                    relayInfo.socket()));
                        } catch (Throwable throwable) {
                            throwable(throwable);
                        }
                    });
                } catch (Throwable throwable) {
                    throwable(throwable);
                }

            } else {
                try {
                    Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);
                    Objects.requireNonNull(msg);

                    if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                        throwable(new Exception("Malformed message"));
                        return;
                    }

                    if (msg.getStatus() != Circuit.Status.OK) {
                        throwable(new Exception("No reservation reason " + msg.getStatus().name()));
                        return;
                    }

                    Limit limit = RelayService.getLimit(msg);
                    Objects.requireNonNull(limit);
                    // Note the limit will not be checked even it is a non limited relay
                    // always a hole punch will be done

                    initializeConnect(stream);
                    stream.setAttribute(INITIALIZED, true);
                } catch (Throwable throwable) {
                    throwable(throwable);
                }
            }
        }


        @NonNull
        private SyncInfo sendSync(Stream stream, byte[] data) throws Exception {

            Long timer = (Long) stream.getAttribute(StreamRequester.TIMER); // timer set earlier
            Objects.requireNonNull(timer, "Timer not set on stream");
            long rtt = System.currentTimeMillis() - timer;

            // B receives the Connect message from A
            Holepunch.HolePunch msg = Holepunch.HolePunch.parseFrom(data);
            Objects.requireNonNull(msg, "Message is not defined");

            if (msg.getType() != Holepunch.HolePunch.Type.CONNECT) {
                throw new Exception("[A] send wrong message connect payloadType, abort");
            }

            Peeraddrs peeraddrs = Peeraddr.create(relayInfo.target(), msg.getObsAddrsList());

            if (peeraddrs.isEmpty()) {
                throw new Exception("[A] send no observed addresses, abort");
            }

            Holepunch.HolePunch response = Holepunch.HolePunch.newBuilder().
                    setType(Holepunch.HolePunch.Type.SYNC).build();
            stream.writeOutput(Utils.encode(response), false);

            Peeraddr peeraddr = Peeraddrs.reduceToOne(peeraddrs);
            Objects.requireNonNull(peeraddr);
            return new SyncInfo(peeraddr, rtt);
        }

        void initializeConnect(Stream stream) {
            Holepunch.HolePunch.Builder builder = Holepunch.HolePunch.newBuilder()
                    .setType(Holepunch.HolePunch.Type.CONNECT);

            for (Peeraddr peeraddr : relayInfo.peeraddrs()) {
                builder.addObsAddrs(ByteString.copyFrom(peeraddr.encoded()));
            }

            Holepunch.HolePunch message = builder.build();
            stream.setAttribute(StreamRequester.TIMER, System.currentTimeMillis());
            stream.writeOutput(Utils.encode(message), false);

        }

        record SyncInfo(Peeraddr peeraddr, long rtt) {
        }


    }


    record ReserveRequest(CompletableFuture<Reservation> done,
                          PeerId self) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void protocol(Stream stream, Protocol protocol) throws Exception {

            if (!Arrays.asList(Protocol.MULTISTREAM_PROTOCOL,
                    Protocol.RELAY_PROTOCOL_HOP).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void fin(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream finished before data"));
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            Circuit.HopMessage msg = Circuit.HopMessage.parseFrom(data);
            if (msg.getType() != Circuit.HopMessage.Type.STATUS) {
                throwable(new Exception("NO RESERVATION " + msg));
                return;
            }
            if (msg.getStatus() != Circuit.Status.OK) {
                throwable(new Exception("NO RESERVATION " + msg));
                return;
            }
            if (!msg.hasReservation()) {
                throwable(new Exception("NO RESERVATION " + msg));
                return;
            }
            Circuit.Reservation reserve = msg.getReservation();
            Limit limit = getLimit(msg);

            Reservation reservation = new Reservation(stream.connection().remotePeeraddr(),
                    limit, reserve.getExpire());

            done.complete(reservation);

        }
    }

    record RelayInfo(@NonNull PeerId target, @NonNull Peeraddrs peeraddrs,
                     @NonNull DatagramSocket socket, @NonNull Parameters parameters) {

    }
}