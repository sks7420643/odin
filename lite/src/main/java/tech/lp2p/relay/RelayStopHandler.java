package tech.lp2p.relay;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Objects;

import circuit.pb.Circuit;
import holepunch.pb.Holepunch;
import tech.lp2p.core.Handler;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Server;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.utils.Utils;

public final class RelayStopHandler implements Handler {
    @NonNull
    private final Server server;

    public RelayStopHandler(@NonNull Server server) {
        this.server = server;
    }

    private static void handleConnectStop(Stream stream, Circuit.StopMessage stopMessage) {

        if (stopMessage.hasPeer()) {
            PeerId peerId = PeerId.create(stopMessage.getPeer().getId().toByteArray());
            Circuit.StopMessage.Builder builder =
                    Circuit.StopMessage.newBuilder()
                            .setType(Circuit.StopMessage.Type.STATUS);
            builder.setStatus(Circuit.Status.OK);

            stream.writeOutput(Utils.encode(builder.build()), false);
            stream.setAttribute(StreamRequester.PEER, peerId);
        } else {
            createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
        }

    }

    private static void createStatusMessage(Stream stream, Circuit.Status status) {
        Circuit.StopMessage.Builder builder =
                Circuit.StopMessage.newBuilder()
                        .setType(Circuit.StopMessage.Type.STATUS);
        builder.setStatus(status);
        stream.writeOutput(Utils.encode(builder.build()), true);
    }

    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(Utils.encodeProtocols(Protocol.RELAY_PROTOCOL_STOP), false);
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {

        if (stream.hasAttribute(StreamRequester.PEER)) {
            try {
                holePunchResponse(stream, data);
            } catch (Throwable throwable) {
                RelayService.closeConnection(stream, throwable.getMessage());
            }
        } else {
            try {
                Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(data);
                Objects.requireNonNull(stopMessage);

                if (stopMessage.getType() == Circuit.StopMessage.Type.CONNECT) {
                    handleConnectStop(stream, stopMessage);
                } else {
                    createStatusMessage(stream, Circuit.Status.MALFORMED_MESSAGE);
                }
            } catch (Throwable throwable) {
                createStatusMessage(stream, Circuit.Status.UNEXPECTED_MESSAGE);
            }
        }
    }

    private void holePunchResponse(Stream stream, byte[] data) throws Exception {

        PeerId target = (PeerId) stream.getAttribute(StreamRequester.PEER);
        Objects.requireNonNull(target);

        Holepunch.HolePunch holePunch = Holepunch.HolePunch.parseFrom(data);

        switch (holePunch.getType()) {
            case CONNECT -> {
                Peeraddrs peeraddrs = Peeraddr.create(target, holePunch.getObsAddrsList());

                if (peeraddrs.isEmpty()) {
                    throw new Exception("Received empty peeraddrs");
                }
                stream.setAttribute(StreamRequester.ADDRS, peeraddrs);

                Holepunch.HolePunch.Builder builder =
                        Holepunch.HolePunch.newBuilder()
                                .setType(Holepunch.HolePunch.Type.CONNECT);

                for (Peeraddr peeraddr : Peeraddr.peeraddrs(server.self(), server.port())) {
                    builder.addObsAddrs(ByteString.copyFrom(peeraddr.encoded()));
                }

                stream.writeOutput(Utils.encode(builder.build()), false);
            }
            case SYNC -> {
                Peeraddrs peeraddrs = (Peeraddrs) stream.getAttribute(StreamRequester.ADDRS);
                Objects.requireNonNull(peeraddrs, "No Peeraddrs"); // should not happen

                Peeraddr peeraddr = Peeraddrs.reduceToOne(peeraddrs);
                Objects.requireNonNull(peeraddr);

                server.holePunching(peeraddr);
            }
        }
    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        Utils.error(getClass().getSimpleName(), "fin received " +
                stream.connection().remotePeerId()); // todo

    }

}
