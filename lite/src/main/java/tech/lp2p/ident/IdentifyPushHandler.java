package tech.lp2p.ident;

import identify.pb.IdentifyOuterClass;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Protocol;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class IdentifyPushHandler implements Handler {


    public IdentifyPushHandler() {
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(Utils.encodeProtocols(Protocol.IDENTITY_PUSH_PROTOCOL), true);
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        // just read out the identity (later store them into the connection
        // when a use-case is there

        // TODO [Future low] when invoked store them on the connection or somewhere else
        IdentifyOuterClass.Identify identify = IdentifyOuterClass.Identify.parseFrom(data);
        IdentifyService.identify(identify, stream.connection().remotePeerId());

    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
        Utils.error(getClass().getSimpleName(), stream.connection().remotePeerId().toString());
    }

}
