package tech.lp2p.ident;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.security.KeyPair;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.quic.Stream;
import tech.lp2p.quic.StreamRequester;
import tech.lp2p.utils.Utils;

public interface IdentifyService {
    String PROTOCOL_VERSION = "ipfs/0.1.0";

    @NonNull
    static IdentifyOuterClass.Identify identify(@NonNull KeyPair keyPair, @NonNull String agent,
                                                @NonNull Set<Protocol> protocols, @NonNull Set<Peeraddr> peeraddrs) {

        Crypto.PublicKey cryptoKey = Crypto.PublicKey.newBuilder()
                .setType(Crypto.KeyType.Ed25519)
                .setData(ByteString.copyFrom(keyPair.getPublic().getEncoded()))
                .build();

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(agent)
                .setPublicKey(ByteString.copyFrom(cryptoKey.toByteArray()))
                .setProtocolVersion(PROTOCOL_VERSION);

        if (!peeraddrs.isEmpty()) {
            for (Peeraddr addr : peeraddrs) {
                builder.addListenAddrs(ByteString.copyFrom(addr.encoded()));
            }
        }

        for (Protocol protocol : protocols) {
            if (protocol.alpn() == ALPN.libp2p) {
                builder.addProtocols(protocol.name());
            }
        }
        return builder.build();
    }

    @NonNull
    static Identify identify(Connection connection) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("ALPN must be libp2p");
        }
        CompletableFuture<IdentifyOuterClass.Identify> done = new CompletableFuture<>();

        StreamRequester.createStream((tech.lp2p.quic.Connection) connection, new IdentityRequest(done))
                .request(Utils.encodeProtocols(Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.IDENTITY_PROTOCOL), Lite.DEFAULT_REQUEST_TIMEOUT);

        IdentifyOuterClass.Identify identify =
                done.get(Lite.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
        return identify(identify, connection.remotePeerId());
    }

    @NonNull
    static Identify identify(IdentifyOuterClass.Identify identify, PeerId expected)
            throws Exception {

        String agent = identify.getAgentVersion();


        Keys pk = Keys.unmarshalPublicKey(identify.getPublicKey().toByteArray());
        PeerId peerId = Keys.fromPubKey(pk);

        if (!Objects.equals(peerId, expected)) {
            throw new Exception("PeerId is not what is expected");
        }


        String[] protocols = new String[identify.getProtocolsCount()];
        identify.getProtocolsList().toArray(protocols);

        Peeraddrs peeraddrs = Peeraddr.create(peerId, identify.getListenAddrsList());
        Peeraddr[] addresses = new Peeraddr[peeraddrs.size()];
        peeraddrs.toArray(addresses);

        return new Identify(peerId, agent, addresses, protocols);
    }

    record IdentityRequest(
            CompletableFuture<IdentifyOuterClass.Identify> done) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, Protocol protocol) throws Exception {
            if (!Arrays.asList(Protocol.MULTISTREAM_PROTOCOL, Protocol.IDENTITY_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            done.complete(IdentifyOuterClass.Identify.parseFrom(data));
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void fin(Stream stream) {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream finished before data"));
            }
        }

    }
}
