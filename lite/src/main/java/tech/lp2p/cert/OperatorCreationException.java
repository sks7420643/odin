package tech.lp2p.cert;

class OperatorCreationException extends OperatorException {
    OperatorCreationException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
