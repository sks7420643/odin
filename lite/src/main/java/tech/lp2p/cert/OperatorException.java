package tech.lp2p.cert;

class OperatorException extends Exception {
    private final Throwable cause;

    OperatorException(String msg, Throwable cause) {
        super(msg);

        this.cause = cause;
    }

    public Throwable getCause() {
        return cause;
    }
}
