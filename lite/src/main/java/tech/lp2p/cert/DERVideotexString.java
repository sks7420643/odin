package tech.lp2p.cert;

final class DERVideotexString extends ASN1VideotexString {
    DERVideotexString(byte[] contents) {
        super(contents);
    }
}
