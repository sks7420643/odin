package tech.lp2p.cert;

abstract class ASN1Util {


    /*
     * Tag text methods
     */

    public static String getTagText(int tagClass, int tagNo) {
        if (tagClass == BERTags.CONTEXT_SPECIFIC) {
            return "[CONTEXT " + tagNo + "]";
        }
        return "[UNIVERSAL " + tagNo + "]";
    }


}
