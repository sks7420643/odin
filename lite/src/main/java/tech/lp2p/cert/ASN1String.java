package tech.lp2p.cert;

/**
 * General interface implemented by ASN.1 STRING objects for extracting the content String.
 */
public interface ASN1String {
    /**
     * Return a Java String representation of this STRING payloadType's content.
     *
     * @return a Java String representation of this STRING.
     */
    String getString();
}
