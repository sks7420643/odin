package tech.lp2p.quic;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

public record Version(int versionId) {

    public static final Version QUIC_version_1 = new Version(0x00000001);
    public static final Version QUIC_version_2 = new Version(0x709a50c4);

    public static Version parse(int input) {
        return new Version(input);
    }

    public byte[] getBytes() {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(versionId);
        return buffer.array();
    }

    public boolean isV2() {
        return versionId == 0x709a50c4;
    }

    public boolean isV1V2() {
        return versionId == 0x00000001 || versionId == 0x709a50c4;
    }


    public int getId() {
        return versionId;
    }

    @NonNull
    @Override
    public String toString() {
        String versionString;
        switch (versionId) {
            case 0x00000001 -> versionString = "v1";
            case 0x709a50c4 -> versionString = "v2";
            default -> {
                if (versionId > 0xff000000 && versionId <= 0xff000022) {
                    versionString = "draft-" + (versionId - 0xff000000);
                } else {
                    versionString = "v-" + Integer.toHexString(versionId);
                }
            }
        }
        return versionString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Version version)) return false;

        return versionId == version.versionId;
    }

}
