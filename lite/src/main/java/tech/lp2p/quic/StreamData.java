package tech.lp2p.quic;

public record StreamData(Stream stream, byte[] data, boolean isFinal, boolean isTerminated) {
}
