package tech.lp2p.quic;


import androidx.annotation.NonNull;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Protocol;

public interface StreamRequester {
    String PEER = "PEER";
    String RELAYED = "RELAYED";

    String STREAM = "STREAM";
    String ADDRS = "ADDRS";
    String TIMER = "TIMER";
    String INITIALIZED = "INITIALIZED";

    @NonNull
    static Stream createStream(@NonNull Connection connection, @NonNull StreamRequester streamRequester)
            throws InterruptedException, TimeoutException {
        if (Objects.equals(connection.alpn(), ALPN.libp2p)) {
            return connection.createStream(AlpnLibp2pRequester.create(streamRequester),
                    true);
        }
        if (Objects.equals(connection.alpn(), ALPN.lite)) {
            return connection.createStream(AlpnLiteRequester.create(streamRequester),
                    true);
        }
        throw new InterruptedException("not supported alpn");
    }

    @NonNull
    static Stream createStream(@NonNull Connection connection)
            throws InterruptedException, TimeoutException {
        return connection.createStream(true);
    }

    void throwable(Throwable throwable);

    void protocol(Stream stream, Protocol protocol) throws Exception;

    void data(Stream stream, byte[] data) throws Exception;

    void terminated();

    void fin(Stream stream);

}
