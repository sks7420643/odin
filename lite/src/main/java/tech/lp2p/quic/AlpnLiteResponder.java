package tech.lp2p.quic;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

import lite.pb.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Responder;
import tech.lp2p.utils.Utils;


public record AlpnLiteResponder(@NonNull Responder responder) implements Consumer<StreamData> {
    private static final String TAG = AlpnLiteResponder.class.getSimpleName();


    public static AlpnLiteResponder create(@NonNull Responder responder) {
        return new AlpnLiteResponder(responder);
    }

    @Override
    public void accept(StreamData rawData) {

        Stream stream = rawData.stream();

        if (rawData.isTerminated()) {
            Utils.error(TAG, "Stream " + stream.connection().remotePeerId() +
                    " is terminated");
            return;
        }

        try {
            Lite.ProtoSelect protoSelect = Lite.ProtoSelect.parseFrom(rawData.data());
            byte[] data = protoSelect.getData().toByteArray();
            responder.data(stream, Protocol.codec(ALPN.lite, protoSelect.getProtocol()), data);
        } catch (Throwable throwable) {
            stream.response(); // finish stream
            stream.terminate(); // terminate stream
            // close connection
            stream.connection().close(new TransportError(
                    TransportError.Code.APPLICATION_ERROR, throwable.getMessage()));

        }
    }

}
