package tech.lp2p.quic;

import static tech.lp2p.quic.Role.Server;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.ack_delay_exponent;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.active_connection_id_limit;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.disable_active_migration;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.initial_max_data;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.initial_max_stream_data_bidi_local;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.initial_max_stream_data_bidi_remote;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.initial_max_stream_data_uni;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.initial_max_streams_bidi;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.initial_max_streams_uni;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.initial_source_connection_id;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.max_ack_delay;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.max_datagram_frame_size;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.max_idle_timeout;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.max_udp_payload_size;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.original_destination_connection_id;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.preferred_address;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.retry_source_connection_id;
import static tech.lp2p.quic.TransportParametersExtension.TransportParameterId.stateless_reset_token;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import tech.lp2p.tls.DecodeErrorException;
import tech.lp2p.tls.Extension;

/**
 * <a href="https://www.rfc-editor.org/rfc/rfc9001.html#name-quic-transport-parameters-e">
 * Quic transport parameter TLS extension.</a>
 */
record TransportParametersExtension(Version quicVersion,
                                    TransportParameters params,
                                    Role senderRole) implements Extension {

    private static final int CODEPOINT_IETFDRAFT = 0xffa5;
    private static final int CODEPOINT_V1 = 0x39;

    /**
     * Creates a Quic Transport Parameters Extension for use in a Client Hello.
     */
    public static TransportParametersExtension create(
            Version quicVersion, TransportParameters params, Role senderRole) {


        return new TransportParametersExtension(quicVersion, params, senderRole);
    }

    public static boolean isCodepoint(Version quicVersion, int extensionType) {
        if (quicVersion.isV1V2()) {
            return extensionType == CODEPOINT_V1;
        } else {
            return extensionType == CODEPOINT_IETFDRAFT;
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private static boolean allZero(byte[] data) {
        for (byte datum : data) {
            if (datum != 0) {
                return false;
            }
        }
        return true;
    }

    public static TransportParametersExtension parse(
            Version quicVersion, ByteBuffer buffer, Role senderRole) throws
            DecodeErrorException {


        int extensionType = buffer.getShort() & 0xffff;
        if (!isCodepoint(quicVersion, extensionType)) {
            throw new RuntimeException();  // Must be programming error
        }
        int extensionLength = buffer.getShort();
        int startPosition = buffer.position();

        TransportParameters transportParameters =
                parseTransportParameter(startPosition, extensionLength, buffer);

        int realSize = buffer.position() - startPosition;
        if (realSize != extensionLength) {
            throw new DecodeErrorException("Invalid transport parameter");
        }
        return new TransportParametersExtension(quicVersion, transportParameters, senderRole);
    }

    private static TransportParameters parseTransportParameter(int initPosition,
                                                               int extensionLength,
                                                               ByteBuffer buffer)
            throws DecodeErrorException {

        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-18.2
        // "The default for this parameter is the maximum permitted UDP payload of 65527"
        int maxUdpPayloadSize = Settings.DEFAULT_MAX_UDP_PAYLOAD_SIZE;
        // "If this value is absent, a default value of 3 is assumed (indicating a multiplier of 8)."
        int ackDelayExponent = Settings.DEFAULT_ACK_DELAY_EXPONENT;
        // "If this value is absent, a default of 25 milliseconds is assumed."
        int maxAckDelay = Settings.DEFAULT_MAX_ACK_DELAY;
        // "If this transport parameter is absent, a default of 2 is assumed."
        int activeConnectionIdLimit = Settings.DEFAULT_ACTIVE_CONNECTION_ID_LIMIT;

        TransportParameters.PreferredAddress preferredAddress = null;
        TransportParameters.VersionInformation versionInformation = null;
        byte[] originalDestinationConnectionId = null;
        int maxIdleTimeout = 0;
        int initialMaxData = 0;
        int initialMaxStreamDataBidiLocal = 0;
        int initialMaxStreamDataBidiRemote = 0;
        int initialMaxStreamDataUni = 0;
        int initialMaxStreamsBidi = 0;
        int initialMaxStreamsUni = 0;
        int maxDatagramFrameSize = 0;
        byte[] initialSourceConnectionId = null;
        byte[] retrySourceConnectionId = null;
        byte[] statelessResetToken = null;
        boolean disableMigration = false;


        while (buffer.position() < extensionLength + initPosition) {

            long parameterId = VariableLengthInteger.parseLong(buffer);
            int size = VariableLengthInteger.parse(buffer);
            if (buffer.remaining() < size) {
                throw new DecodeErrorException("Invalid transport parameter extension");
            }
            int startPosition = buffer.position();

            if (parameterId == original_destination_connection_id.value) {
                originalDestinationConnectionId = new byte[size];
                buffer.get(originalDestinationConnectionId);
            } else if (parameterId == max_idle_timeout.value) {
                maxIdleTimeout = VariableLengthInteger.parse(buffer);
            } else if (parameterId == stateless_reset_token.value) {
                statelessResetToken = new byte[16];
                buffer.get(statelessResetToken);

            } else if (parameterId == max_udp_payload_size.value) {
                maxUdpPayloadSize = VariableLengthInteger.parse(buffer);
            } else if (parameterId == initial_max_data.value) {
                initialMaxData = VariableLengthInteger.parse(buffer);
            } else if (parameterId == initial_max_stream_data_bidi_local.value) {
                initialMaxStreamDataBidiLocal = VariableLengthInteger.parse(buffer);
            } else if (parameterId == initial_max_stream_data_bidi_remote.value) {
                initialMaxStreamDataBidiRemote = VariableLengthInteger.parse(buffer);
            } else if (parameterId == initial_max_stream_data_uni.value) {
                initialMaxStreamDataUni = VariableLengthInteger.parse(buffer);
            } else if (parameterId == initial_max_streams_bidi.value) {
                initialMaxStreamsBidi = VariableLengthInteger.parse(buffer);
            } else if (parameterId == initial_max_streams_uni.value) {
                initialMaxStreamsUni = VariableLengthInteger.parse(buffer);
            } else if (parameterId == ack_delay_exponent.value) {
                ackDelayExponent = VariableLengthInteger.parse(buffer);
            } else if (parameterId == max_ack_delay.value) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-30#section-18.2
                // "The maximum acknowledgement delay is an integer value indicating the maximum amount of time in
                //  milliseconds by which the endpoint will delay sending acknowledgments. "
                maxAckDelay = VariableLengthInteger.parse(buffer);
            } else if (parameterId == disable_active_migration.value) {
                disableMigration = true;
            } else if (parameterId == preferred_address.value) {
                preferredAddress = parsePreferredAddress(buffer);
            } else if (parameterId == active_connection_id_limit.value) {
                activeConnectionIdLimit = (int) VariableLengthInteger.parseLong(buffer);
            } else if (parameterId == max_datagram_frame_size.value) {
                maxDatagramFrameSize = VariableLengthInteger.parse(buffer);
            } else if (parameterId == initial_source_connection_id.value) {
                initialSourceConnectionId = new byte[size];
                buffer.get(initialSourceConnectionId);
            } else if (parameterId == retry_source_connection_id.value) {
                retrySourceConnectionId = new byte[size];
                buffer.get(retrySourceConnectionId);
            } else if (parameterId == TransportParameterId.version_information.value) {
                // https://www.ietf.org/archive/id/draft-ietf-quic-version-negotiation-05.html#name-version-information
                if (size % 4 != 0 || size < 4) {
                    throw new DecodeErrorException("invalid parameters size");
                }
                int chosenVersion = buffer.getInt();
                List<Version> otherVersions = new ArrayList<>();
                for (int i = 0; i < size / 4 - 1; i++) {
                    int otherVersion = buffer.getInt();
                    otherVersions.add(Version.parse(otherVersion));
                }


                Version[] versions = new Version[otherVersions.size()];
                versionInformation = new TransportParameters.VersionInformation(Version.parse(chosenVersion),
                        otherVersions.toArray(versions));
            } else {

                // Reserved Transport Parameters
                //
                //   Transport parameters with an identifier of the form "31 * N + 27" for
                //   integer values of N are reserved to exercise the requirement that
                //   unknown transport parameters be ignored.  These transport parameters
                //   have no semantics, and can carry and can carry arbitrary values.

                // https://datatracker.ietf.org/doc/html/draft-ietf-quic-transport-34#section-18.1

                buffer.get(new byte[size]);
            }

            int realSize = buffer.position() - startPosition;
            if (realSize != size) {
                throw new DecodeErrorException("inconsistent size in transport parameter");
            }
        }
        return new TransportParameters(preferredAddress, versionInformation,
                originalDestinationConnectionId,
                maxIdleTimeout, initialMaxData, initialMaxStreamDataBidiLocal,
                initialMaxStreamDataBidiRemote, initialMaxStreamDataUni, initialMaxStreamsBidi,
                initialMaxStreamsUni, ackDelayExponent, maxAckDelay, activeConnectionIdLimit,
                maxUdpPayloadSize, initialSourceConnectionId, retrySourceConnectionId,
                statelessResetToken, disableMigration, maxDatagramFrameSize,
                (int) Math.pow(2, ackDelayExponent));
    }

    private static TransportParameters.PreferredAddress parsePreferredAddress(ByteBuffer buffer)
            throws DecodeErrorException {
        try {

            byte[] ip4 = new byte[4];
            buffer.get(ip4);
            InetAddress inet4 = null;
            if (!allZero(ip4)) {
                inet4 = InetAddress.getByAddress(ip4);
            }
            int portIp4 = ((buffer.get() << 8) | buffer.get());
            byte[] ip6 = new byte[16];
            buffer.get(ip6);
            InetAddress inet6 = null;
            if (!allZero(ip6)) {
                inet6 = InetAddress.getByAddress(ip6);
            }
            int portIp6 = ((buffer.get() << 8) | buffer.get());

            if (inet4 == null && inet6 == null) {
                throw new DecodeErrorException("Invalid preferred address");
            }

            int connectionIdSize = buffer.get();

            byte[] connectionId = new byte[connectionIdSize];
            buffer.get(connectionId);

            byte[] statelessResetToken = new byte[16];
            buffer.get(statelessResetToken);

            return new TransportParameters.PreferredAddress(inet4, portIp4,
                    inet6, portIp6, connectionId, statelessResetToken);


        } catch (UnknownHostException unknownHostException) {
            throw new DecodeErrorException(unknownHostException.getMessage());
        }
    }

    private static void addTransportParameter(ByteBuffer buffer, TransportParameterId id, long value) {
        addTransportParameter(buffer, id.value, value);
    }

    private static void addTransportParameter(ByteBuffer buffer, int id, long value) {
        VariableLengthInteger.encode(id, buffer);
        buffer.mark();
        int encodedValueLength = VariableLengthInteger.encode(value, buffer);
        buffer.reset();
        VariableLengthInteger.encode(encodedValueLength, buffer);
        VariableLengthInteger.encode(value, buffer);
    }

    private static void addTransportParameter(ByteBuffer buffer, TransportParameterId id, byte[] value) {
        addTransportParameter(buffer, id.value, value);
    }

    private static void addTransportParameter(ByteBuffer buffer, int id, byte[] value) {
        VariableLengthInteger.encode(id, buffer);
        VariableLengthInteger.encode(value.length, buffer);
        buffer.put(value);
    }

    @Override
    public byte[] getBytes() {
        ByteBuffer buffer = ByteBuffer.allocate(1024);

        // https://tools.ietf.org/html/draft-ietf-quic-tls-32#section-8.2
        // "quic_transport_parameters(0xffa5)"
        buffer.putShort((short) (quicVersion.equals(Version.QUIC_version_1) || quicVersion.isV2() ? CODEPOINT_V1 : CODEPOINT_IETFDRAFT));

        // Format is same as any TLS extension, so next are 2 bytes length
        buffer.putShort((short) 0);  // PlaceHolder, will be correctly set at the end of this method.

        // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-18.2
        // "Those transport parameters that are identified as integers use a variable-length integer encoding (...) and
        //  have a default value of 0 if the transport parameter is absent, unless otherwise stated."

        if (senderRole == Server) {
            // "The value of the Destination Connection ID field from the first Initial packet sent by the client (...)
            // This transport parameter is only sent by a server."
            addTransportParameter(buffer, original_destination_connection_id, params.originalDestinationConnectionId());
        }

        // "The max idle timeout is a value in milliseconds that is encoded as an integer"
        addTransportParameter(buffer, max_idle_timeout, params.maxIdleTimeout());

        if (senderRole == Server && params.statelessResetToken() != null) {
            // "A stateless reset token is used in verifying a stateless reset (...). This parameter is a sequence of 16
            //  bytes. This transport parameter MUST NOT be sent by a client, but MAY be sent by a server."
            addTransportParameter(buffer, stateless_reset_token, params.statelessResetToken());
        }

        // "The maximum UDP payload size parameter is an integer value that limits the size of UDP payloads that the
        //  endpoint is willing to receive.  UDP datagrams with payloads larger than this limit are not likely to be
        //  processed by the receiver."
        addTransportParameter(buffer, max_udp_payload_size, params.maxUdpPayloadSize());

        // "The initial maximum data parameter is an integer value that contains the initial value for the maximum
        //  amount of data that can be sent on the connection.  This is equivalent to sending a MAX_DATA for the
        //  connection immediately after completing the handshake."
        addTransportParameter(buffer, initial_max_data, params.initialMaxData());

        // "This parameter is an integer value specifying the initial flow control limit for locally-initiated
        //  bidirectional streams. This limit applies to newly created bidirectional streams opened by the endpoint that
        //  sends the transport parameter."
        addTransportParameter(buffer, initial_max_stream_data_bidi_local, params.initialMaxStreamDataBidiLocal());

        // "This parameter is an integer value specifying the initial flow control limit for peer-initiated bidirectional
        //  streams. This limit applies to newly created bidirectional streams opened by the endpoint that receives
        //  the transport parameter."
        addTransportParameter(buffer, initial_max_stream_data_bidi_remote, params.initialMaxStreamDataBidiRemote());

        // "This parameter is an integer value specifying the initial flow control limit for unidirectional streams.
        //  This limit applies to newly created bidirectional streams opened by the endpoint that receives the transport
        //  parameter."
        addTransportParameter(buffer, initial_max_stream_data_uni, params.initialMaxStreamDataUni());

        // "The initial maximum bidirectional streams parameter is an integer value that contains the initial maximum
        //  number of bidirectional streams the peer may initiate.  If this parameter is absent or zero, the peer cannot
        //  open bidirectional streams until a MAX_STREAMS frame is sent."
        addTransportParameter(buffer, initial_max_streams_bidi, params.initialMaxStreamsBidi());

        // "The initial maximum unidirectional streams parameter is an integer value that contains the initial maximum
        //  number of unidirectional streams the peer may initiate. If this parameter is absent or zero, the peer cannot
        //  open unidirectional streams until a MAX_STREAMS frame is sent."
        addTransportParameter(buffer, initial_max_streams_uni, params.initialMaxStreamsUni());

        // "The acknowledgement delay exponent is an integer value indicating an exponent used to decode the ACK Delay
        // field in the ACK frame"
        addTransportParameter(buffer, ack_delay_exponent, params.ackDelayExponent());

        // "The maximum acknowledgement delay is an integer value indicating the maximum amount of time in milliseconds
        //  by which the endpoint will delay sending acknowledgments."
        addTransportParameter(buffer, max_ack_delay, params.maxAckDelay());

        // The max_datagram_frame_size transport parameter is an integer value
        // (represented as a variable-length integer) that represents the maximum size of a
        // DATAGRAM frame (including the frame payloadType, length, and payload) the endpoint is willing
        // to receive, in bytes. An endpoint that includes this parameter supports the DATAGRAM
        // frame types and is willing to receive such frames on this connection.
        addTransportParameter(buffer, max_datagram_frame_size, params.maxDatagramFrameSize());

        // Intentionally omitted (kwik server supports active migration)
        // disable_active_migration

        // Intentionally omitted (kwik server does not support preferred address)
        // preferred_address

        // "The maximum number of connection IDs from the peer that an endpoint is willing to store."
        addTransportParameter(buffer, active_connection_id_limit, params.activeConnectionIdLimit());

        // "The value that the endpoint included in the Source Connection ID field of the first Initial packet it
        //  sends for the connection"
        addTransportParameter(buffer, initial_source_connection_id, params.initialSourceConnectionId());

        if (senderRole == Server) {
            // "The value that the the server included in the Source Connection ID field of a Retry packet"
            // "This transport parameter is only sent by a server."
            if (params.retrySourceConnectionId() != null) {
                addTransportParameter(buffer, retry_source_connection_id, params.retrySourceConnectionId());
            }
        }


        if (params.versionInformation() != null) {
            TransportParameters.VersionInformation versions = params.versionInformation();
            ByteBuffer data = ByteBuffer.allocate(4 + versions.otherVersions().length * 4);
            data.put(versions.chosenVersion().getBytes());
            for (Version version : versions.otherVersions()) {
                data.put(version.getBytes());
            }
            addTransportParameter(buffer, TransportParameterId.version_information, data.array());
        }

        int length = buffer.position();
        buffer.limit(length);

        int extensionsSize = length - 2 - 2;  // 2 bytes for the length itself and 2 for the payloadType
        buffer.putShort(2, (short) extensionsSize);

        byte[] data = new byte[length];
        buffer.flip();
        buffer.get(data);
        return data;
    }

    public TransportParameters getTransportParameters() {
        return params;
    }


// https://www.rfc-editor.org/rfc/rfc9000.html#name-transport-parameter-definit

    // 0x00	original_destination_connection_id	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x01	max_idle_timeout	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x02	stateless_reset_token	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x03	max_udp_payload_size	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x04	initial_max_data	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x05	initial_max_stream_data_bidi_local	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x06	initial_max_stream_data_bidi_remote	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x07	initial_max_stream_data_uni	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x08	initial_max_streams_bidi	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x09	initial_max_streams_uni	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x0a	ack_delay_exponent	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x0b	max_ack_delay	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x0c	disable_active_migration	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x0d	preferred_address	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x0e	active_connection_id_limit	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x0f	initial_source_connection_id	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x10	retry_source_connection_id	permanent	[RFC9000, Section 18.2]	2021-02-11	IETF	[QUIC_WG]
//0x11	version_information	permanent	[RFC9368]	2022-12-16	IETF	[QUIC_WG]
//0x20	max_datagram_frame_size	permanent	[RFC9221]	2021-10-20	IETF	[QUIC_WG]
//0x173e	discard	provisional	[https://github.com/quicwg/base-drafts/wiki/Quantum-Readiness-test]	2022-06-02	[David_Schinazi]	[David_Schinazi]	Receiver silently discards.
//0x26ab	google handshake message	provisional		2022-11-01	Google	[Google]	Used to carry Google internal handshake message
//0x2ab2	grease_quic_bit	permanent	[RFC9287]	2022-07-13	IETF	[QUIC_WG]
//0x3127	initial_rtt	provisional		2021-10-20	Google	[Google]	Initial RTT in microseconds
//0x3128	google_connection_options	provisional		2021-10-20	Google	[Google]	Google connection options for experimentation
//0x3129	user_agent	provisional		2021-10-20	Google	[Google]	User agent string (deprecated)
//0x4752	google_version	provisional		2021-10-20	Google	[Google]	Google QUIC version downgrade prevention
//0x0f739bbc1b666d05	enable_multipath	provisional	[draft-ietf-quic-multipath-05, Section 3]	2023-07-26	Yanmei Liu	[Yanmei_Liu]
    enum TransportParameterId {

        original_destination_connection_id(0),
        max_idle_timeout(1),
        stateless_reset_token(2),
        max_udp_payload_size(3),
        initial_max_data(4),
        initial_max_stream_data_bidi_local(5),
        initial_max_stream_data_bidi_remote(6),
        initial_max_stream_data_uni(7),
        initial_max_streams_bidi(8),
        initial_max_streams_uni(9),
        ack_delay_exponent(0x0a),
        max_ack_delay(0x0b),
        disable_active_migration(0x0c),
        preferred_address(0x0d),
        active_connection_id_limit(0x0e),
        initial_source_connection_id(0x0f),
        retry_source_connection_id(0x10),
        // https://www.ietf.org/archive/id/draft-ietf-quic-version-negotiation-05.html#name-quic-transport-parameter
        // // https://www.iana.org/assignments/quic/quic.xhtml
        version_information(0x11),

        max_datagram_frame_size(0x0020);
        public final int value;

        TransportParameterId(int value) {
            this.value = value;
        }
    }

}
