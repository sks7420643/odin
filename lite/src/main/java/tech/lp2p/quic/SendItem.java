package tech.lp2p.quic;


import java.util.function.Consumer;

record SendItem(Packet packet, Consumer<Packet> packetLostCallback) {
}

