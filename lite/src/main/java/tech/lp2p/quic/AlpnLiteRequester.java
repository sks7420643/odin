package tech.lp2p.quic;

import androidx.annotation.NonNull;

import java.util.function.Consumer;

import tech.lp2p.utils.Utils;


public record AlpnLiteRequester(
        @NonNull StreamRequester streamRequester) implements Consumer<StreamData> {
    private static final String TAG = AlpnLiteRequester.class.getSimpleName();


    public static AlpnLiteRequester create(@NonNull StreamRequester streamRequester) {
        return new AlpnLiteRequester(streamRequester);
    }

    @Override
    public void accept(StreamData rawData) {

        Stream stream = rawData.stream();

        if (rawData.isTerminated()) {
            streamRequester.terminated();
            return;
        }

        try {

            byte[] data = rawData.data();
            if (data.length > 0) {
                streamRequester.data(stream, data);

                if (rawData.isFinal()) {
                    streamRequester.fin(stream);
                }

            } else {
                streamRequester.fin(stream);
            }

        } catch (Throwable throwable) {
            Utils.error(TAG, "stream reading exception " + throwable.getMessage());
            stream.terminate();
            streamRequester.throwable(throwable);
        }
    }

}
