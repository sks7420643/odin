package tech.lp2p.quic;


import androidx.annotation.Nullable;

import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;


/**
 * Listens for received packets and generates ack frames for them.
 */
final class AckGenerator {

    private static final int ACK_FREQUENCY_TWO = 2;
    private static final int TIMING_OFFSET = 6; // some timing offset

    // TODO [Future low] not using Settings.MAX_ACK_DELAY, instead using the max ack delay
    // TODO [Future low] provided by the remote transport parameters [not yet important]
    private static final int ACK_DELAY = Settings.MAX_ACK_DELAY - TIMING_OFFSET;

    private final ConcurrentSkipListSet<Long> rangesToAcknowledge =
            new ConcurrentSkipListSet<>(Long::compareTo);
    private final ConcurrentSkipListMap<Long, Long[]> ackSentWithPacket =
            new ConcurrentSkipListMap<>(Long::compareTo);
    private final AtomicLong newPacketsToAcknowlegdeSince = new AtomicLong(Settings.NOT_DEFINED);
    private final AtomicInteger acksNotSend = new AtomicInteger(0);


    // only invoked from the SENDER THREAD
    boolean mustSendAck(Level level) {
        long acknowlegdeSince = newPacketsToAcknowlegdeSince.get();
        if (acknowlegdeSince != Settings.NOT_DEFINED) {
            // precondition that new packages have arrived
            if (level == Level.App) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-13.2.2
                // "A receiver SHOULD send an ACK frame after receiving at least two
                // ack-eliciting packets."

                if (acksNotSend.get() >= ACK_FREQUENCY_TWO) {
                    return true;
                } else {
                    // NOTE: here is the case for 1 acknowledged packet
                    // check when the current time is greater then the first arrived package
                    // plus max ack delay, then a ack must send (returns true)
                    // Note: ACK_DELAY is  Settings.MAX_ACK_DELAY - TIMING_DELAY
                    return System.currentTimeMillis() > acknowlegdeSince
                            + ACK_DELAY;
                }
            }
            return true; // for other level it is always without any delay
        }
        return false;
    }

    // only invoked from the SENDER THREAD
    int nextDelayedSend() {
        if (newPacketsToAcknowlegdeSince.get() != Settings.NOT_DEFINED) {
            return ACK_DELAY;
        }
        return Settings.NOT_DEFINED;
    }


    // only invoked from the SENDER THREAD
    void cleanup() {
        rangesToAcknowledge.clear();
        ackSentWithPacket.clear();
        acksNotSend.set(0);
        newPacketsToAcknowlegdeSince.set(Settings.NOT_DEFINED);
    }


    // only invoked from the RECEIVER THREAD
    void packetReceived(PacketReceived packetReceived, long timeReceived) {
        rangesToAcknowledge.add(packetReceived.packetNumber());

        if (PacketReceived.isAckEliciting(packetReceived)) {

            newPacketsToAcknowlegdeSince.updateAndGet(operand -> {
                if (operand == Settings.NOT_DEFINED) {
                    return timeReceived;
                }
                return operand;
            });

            if (packetReceived.level() == Level.App) {
                acksNotSend.incrementAndGet();
            }
        }
    }


    // only invoked from the RECEIVER THREAD
    void ackFrameReceived(FrameReceived.AckFrame receivedAck) {

        // Find max packet number that had an ack sent with it...
        long largestWithAck = -1;

        // it is ordered, packet status with highest packet number is at the top
        long[] acknowledgedRanges = receivedAck.acknowledgedRanges();
        for (int i = 0; i < acknowledgedRanges.length; i++) {
            long to = acknowledgedRanges[i];
            i++;
            long from = acknowledgedRanges[i];
            for (long pn = to; pn >= from; pn--) {
                if (ackSentWithPacket.containsKey(pn)) {
                    largestWithAck = pn;
                    break;
                }
            }
            if (largestWithAck > 0) {
                break;
            }
        }


        if (largestWithAck > 0) {

            // ... and for that max pn, all packets that where acked by it don't need to be acked again.
            Long[] latestAcknowledgedAck = ackSentWithPacket.get(largestWithAck);

            if (latestAcknowledgedAck != null) {
                for (Long packet : latestAcknowledgedAck) {
                    rangesToAcknowledge.remove(packet);
                }
            }
            // And for all earlier sent packets (smaller packet numbers), the sent ack's can be discarded because
            // their ranges are a subset of the ones from the latestAcknowledgedAck and thus are now implicitly acked.

            Set<Long> oldEntries = ackSentWithPacket.headMap(largestWithAck).keySet();
            for (Long old : oldEntries) {
                ackSentWithPacket.remove(old);
            }
        }
    }

    // only invoked from the SENDER THREAD
    @Nullable
    Frame generateAck(long packetNumber, Function<Integer, Boolean> acceptLength) {

        if (rangesToAcknowledge.isEmpty()) {
            return null;
        } else {
            int delay = 0;
            // https://tools.ietf.org/html/draft-ietf-quic-transport-34#section-13.2.1
            // "An endpoint MUST acknowledge all ack-eliciting Initial and Handshake packets immediately"
            long acknowlegdeSince = newPacketsToAcknowlegdeSince.get();
            if (acknowlegdeSince != Settings.NOT_DEFINED) {
                delay = Math.min((int) (System.currentTimeMillis() - acknowlegdeSince), 0);
            }

            //noinspection SimplifyStreamApiCallChains
            Long[] packets = rangesToAcknowledge.stream().toArray(Long[]::new);

            // Range list must not be modified during frame initialization (guaranteed by this method being sync'd)
            Frame frame = Frame.createAckFrame(packets, delay);
            if (acceptLength.apply(frame.frameLength())) {
                ackSentWithPacket.put(packetNumber, packets);
                newPacketsToAcknowlegdeSince.set(Settings.NOT_DEFINED);
                acksNotSend.set(0);
                return frame;
            } else {
                return null;
            }
        }
    }
}

