/*
 * Copyright © 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

import java.nio.ByteBuffer;

import tech.lp2p.utils.Utils;

// https://tools.ietf.org/html/rfc8446#section-4.3.2
public record CertificateRequestMessage(byte[] certificateRequestContext,
                                        Extension[] extensions,
                                        byte[] raw) implements HandshakeMessage {

    private static final int MINIMUM_MESSAGE_SIZE = 1 + 3 + 1 + 2;


    static CertificateRequestMessage createCertificateRequestMessage(Extension extension) {
        Extension[] extensions = new Extension[1];
        extensions[0] = extension;
        byte[] certificateRequestContext = Utils.BYTES_EMPTY;

        int extensionsLength = extension.getBytes().length;
        int messageLength = 4 + 1 + certificateRequestContext.length + 2 + extensionsLength;
        ByteBuffer buffer = ByteBuffer.allocate(messageLength);
        buffer.put(HandshakeType.certificate_request.value);
        buffer.put((byte) 0x00);
        buffer.putShort((short) (messageLength - 4));
        buffer.put((byte) certificateRequestContext.length);
        buffer.putShort((short) extensionsLength);
        for (Extension ext : extensions) {
            buffer.put(ext.getBytes());
        }
        byte[] raw = buffer.array();
        return new CertificateRequestMessage(certificateRequestContext, extensions, raw);
    }

    public static CertificateRequestMessage parse(ByteBuffer buffer) throws ErrorAlert {
        int startPosition = buffer.position();
        int remainingLength = HandshakeMessage.parseHandshakeHeader(buffer, HandshakeType.certificate_request, MINIMUM_MESSAGE_SIZE);

        int contextLength = buffer.get();
        byte[] certificateRequestContext = new byte[contextLength];
        if (contextLength > 0) {
            buffer.get(certificateRequestContext);
        }

        Extension[] extensions = HandshakeMessage.parseExtensions(
                buffer, HandshakeType.certificate_request, null);

        if (buffer.position() - (startPosition + 4) != remainingLength) {
            throw new DecodeErrorException("inconsistent length");
        }

        // Update state.
        byte[] raw = new byte[4 + remainingLength];
        buffer.position(startPosition);
        buffer.get(raw);

        return new CertificateRequestMessage(certificateRequestContext, extensions, raw);
    }


    @Override
    public HandshakeType getType() {
        return HandshakeType.certificate_request;
    }

    @Override
    public byte[] getBytes() {
        return raw;
    }

}
