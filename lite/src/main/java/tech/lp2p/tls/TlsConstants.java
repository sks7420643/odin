/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.tls;

public interface TlsConstants {


    enum ExtensionType {
        server_name(0),                             /* RFC 6066 */
        /**
         * @noinspection unused
         */
        max_fragment_length(1),                     /* RFC 6066 */
        /**
         * @noinspection unused
         */
        status_request(5),                          /* RFC 6066 */
        supported_groups(10),                       /* RFC 8422, 7919 */
        signature_algorithms(13),                   /* RFC 8446 */
        /**
         * @noinspection unused
         */
        use_srtp(14),                               /* RFC 5764 */
        /**
         * @noinspection unused
         */
        heartbeat(15),                              /* RFC 6520 */
        application_layer_protocol_negotiation(16), /* RFC 7301 */
        /**
         * @noinspection unused
         */
        signed_certificate_timestamp(18),           /* RFC 6962 */
        /**
         * @noinspection unused
         */
        client_certificate_type(19),                /* RFC 7250 */
        /**
         * @noinspection unused
         */
        server_certificate_type(20),                /* RFC 7250 */
        /**
         * @noinspection unused
         */
        padding(21),                                /* RFC 7685 */
        pre_shared_key(41),                         /* RFC 8446 */
        early_data(42),                             /* RFC 8446 */
        supported_versions(43),                     /* RFC 8446 */
        /**
         * @noinspection unused
         */
        cookie(44),                                 /* RFC 8446 */
        psk_key_exchange_modes(45),                 /* RFC 8446 */
        certificate_authorities(47),                /* RFC 8446 */
        /**
         * @noinspection unused
         */
        oid_filters(48),                            /* RFC 8446 */
        /**
         * @noinspection unused
         */
        post_handshake_auth(49),                    /* RFC 8446 */
        /**
         * @noinspection unused
         */
        signature_algorithms_cert(50),              /* RFC 8446 */
        key_share(51),
        ;

        /**
         * @noinspection unused
         */
        public final short value;

        ExtensionType(int value) {
            this.value = (short) value;
        }
    }


    enum PskKeyExchangeMode {
        psk_ke(0),
        psk_dhe_ke(1);

        public final byte value;

        PskKeyExchangeMode(int value) {
            this.value = (byte) value;
        }
    }


    enum AlertDescription {
        /**
         * @noinspection unused
         */
        close_notify(0),
        unexpected_message(10),
        /**
         * @noinspection unused
         */
        bad_record_mac(20),
        /**
         * @noinspection unused
         */
        record_overflow(22),
        handshake_failure(40),
        bad_certificate(42),
        /**
         * @noinspection unused
         */
        unsupported_certificate(43),
        /**
         * @noinspection unused
         */
        certificate_revoked(44),
        /**
         * @noinspection unused
         */
        certificate_expired(45),
        certificate_unknown(46),
        illegal_parameter(47),
        /**
         * @noinspection unused
         */
        unknown_ca(48),
        /**
         * @noinspection unused
         */
        access_denied(49),
        decode_error(50),
        decrypt_error(51),
        /**
         * @noinspection unused
         */
        protocol_version(70),
        /**
         * @noinspection unused
         */
        insufficient_security(71),
        internal_error(80),
        /**
         * @noinspection unused
         */
        inappropriate_fallback(86),
        /**
         * @noinspection unused
         */
        user_canceled(90),
        missing_extension(109),
        unsupported_extension(110),
        /**
         * @noinspection unused
         */
        unrecognized_name(112),
        /**
         * @noinspection unused
         */
        bad_certificate_status_response(113),
        /**
         * @noinspection unused
         */
        unknown_psk_identity(115),
        /**
         * @noinspection unused
         */
        certificate_required(116),
        no_application_protocol(120);

        private final byte value;

        AlertDescription(int value) {
            this.value = (byte) value;
        }

        public byte value() {
            return value;
        }
    }
}
