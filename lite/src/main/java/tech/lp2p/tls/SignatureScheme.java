package tech.lp2p.tls;

import java.util.HashMap;
import java.util.Map;

public enum SignatureScheme {
    /* RSASSA-PKCS1-v1_5 algorithms */
    rsa_pkcs1_sha256(0x0401),
    rsa_pkcs1_sha384(0x0501),
    rsa_pkcs1_sha512(0x0601),

    /* ECDSA algorithms */
    ecdsa_secp256r1_sha256(0x0403),
    ecdsa_secp384r1_sha384(0x0503),
    ecdsa_secp521r1_sha512(0x0603),

    /* RSASSA-PSS algorithms with public key OID rsaEncryption */
    rsa_pss_rsae_sha256(0x0804),
    rsa_pss_rsae_sha384(0x0805),
    rsa_pss_rsae_sha512(0x0806),

    /* EdDSA algorithms */
    ed25519(0x0807),
    ed448(0x0808),

    /* RSASSA-PSS algorithms with public key OID RSASSA-PSS */
    rsa_pss_pss_sha256(0x0809),
    rsa_pss_pss_sha384(0x080a),
    rsa_pss_pss_sha512(0x080b),

    /* Legacy algorithms */
    rsa_pkcs1_sha1(0x0201),
    ecdsa_sha1(0x0203),
    ;

    private static final Map<Short, SignatureScheme> byValue = new HashMap<>();

    static {
        for (SignatureScheme t : SignatureScheme.values()) {
            byValue.put(t.value, t);
        }
    }

    public final short value;

    SignatureScheme(int value) {
        this.value = (short) value;
    }


    public static SignatureScheme get(short value) throws DecodeErrorException {
        SignatureScheme signatureScheme = byValue.get(value);
        if (signatureScheme == null) {
            throw new DecodeErrorException("invalid signature scheme value");
        }
        return signatureScheme;
    }
}
