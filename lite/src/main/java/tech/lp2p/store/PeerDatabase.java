package tech.lp2p.store;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = {Peer.class}, version = 2, exportSchema = false)
@TypeConverters({RoomConverters.class})
public abstract class PeerDatabase extends RoomDatabase {

    public abstract PeerDao bootstrapDao();

}
