package tech.lp2p.store;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;


public final class BLOCKS implements BlockStore {

    private static volatile BLOCKS INSTANCE = null;
    private final BlockStoreDatabase blocksStoreDatabase;

    private BLOCKS(BlockStoreDatabase blocksStoreDatabase) {
        this.blocksStoreDatabase = blocksStoreDatabase;
    }

    public static BLOCKS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BLOCKS.class) {
                if (INSTANCE == null) {
                    BlockStoreDatabase blocksStoreDatabase = Room.databaseBuilder(context,
                                    BlockStoreDatabase.class,
                                    BlockStoreDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new BLOCKS(blocksStoreDatabase);
                }
            }
        }
        return INSTANCE;
    }

    public void clear() {
        blocksStoreDatabase.clearAllTables();
    }


    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().hasBlock(cid);
    }

    @Nullable
    @Override
    public byte[] getBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().getData(cid);
    }

    @Override
    public void storeBlock(@NonNull Cid cid, byte[] data) {
        blocksStoreDatabase.blockDao().insertBlock(new Block(cid, data));
    }

    @Override
    public void deleteBlock(@NonNull Cid cid) {
        blocksStoreDatabase.blockDao().deleteBlock(cid);
    }


}
