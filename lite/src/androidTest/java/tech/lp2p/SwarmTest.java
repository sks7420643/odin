package tech.lp2p;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.utils.Utils;

public class SwarmTest {

    private static final String TAG = SwarmTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_find_peer_in_swarm() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!server.hasPublicPeeraddrs()) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        assertFalse(Lite.reservations(server).isEmpty()); // pre-condition (reservations done on swarm)

        // now we check if we can find ourself
        AtomicBoolean found = new AtomicBoolean(false);
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {
            dummySession.findPeer(new TimeoutCancellable(120), multiaddr -> {
                Utils.error(TAG, multiaddr.toString());
                found.set(true);
            }, lite.self()); // find me
        }

        assertTrue(found.get());

    }

    @Test
    public void test_dial_peer_in_swarm() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!server.hasPublicPeeraddrs()) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        assertFalse(Lite.reservations(server).isEmpty()); // pre-condition (reservations done on swarm)

        // now we check if we can dial ourself
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {

            Parameters parameters = Lite.connectionParameters(ALPN.lite);
            Connection connection = Lite.dial(dummySession, lite.self(),
                    parameters, new TimeoutCancellable(120)); // find me

            assertNotNull(connection);

            Envelope entry = Lite.pullEnvelope(dummySession, connection, TestEnv.CID);
            assertNotNull(entry);
        }


    }

    @Test
    public void test_resolve_peer_in_swarm() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!server.hasPublicPeeraddrs()) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        assertFalse(Lite.reservations(server).isEmpty()); // pre-condition (reservations done on swarm)

        // now we check if we can dial ourself
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {
            // normally it should done on dummy (but ok, works on session anyway)
            Parameters parameters = Parameters.create(ALPN.lite);
            Connection connection = Lite.dial(dummySession, peeraddr, parameters);
            Envelope envelope = Lite.pullEnvelope(dummySession, connection, TestEnv.CID);
            assertNotNull(envelope);

            connection.close();
        }


    }
}
