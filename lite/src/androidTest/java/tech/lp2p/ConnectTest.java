package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;
import tech.lp2p.utils.Utils;


public class ConnectTest {
    private static final String TAG = ConnectTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void findPeer() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {

            // todo right peerId
            PeerId peerId = Lite.decodePeerId("QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa");
            assertNotNull(peerId);

            List<Peeraddr> peeraddrList = new ArrayList<>();
            Lite.findPeer(session, peerId, peeraddrList::add, new TimeoutCancellable(120));
            assertFalse(peeraddrList.isEmpty()); // should not be empty

        }

    }

    @Test
    public void dialPeer() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {

            // todo find right address
            String address = "/dnsaddr/bootstrap.libp2p.io/p2p/QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa";
            Peeraddr peeraddr = Lite.decodePeeraddr(address);
            assertNotNull(peeraddr.host());


            Connection connection = Lite.dial(session, peeraddr, Parameters.create(ALPN.libp2p));
            assertTrue(connection.isConnected());


            Identify identify = Lite.identify(connection);
            Utils.error(TAG, identify.toString());

            connection.close();

        }

    }

    @Test(expected = TimeoutException.class)
    public void swarmPeer() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            throw new TimeoutException("nothing to test here NO NETWORK");
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {

            PeerId peerId = Lite.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");

            Peeraddr peeraddr = Lite.createPeeraddr(peerId,
                    InetAddress.getByName("139.178.68.146"), 4001);
            assertNotNull(peeraddr.inetSocketAddress());
            assertEquals(peeraddr.inetSocketAddress().getPort(), peeraddr.port());
            assertEquals(peeraddr.inetSocketAddress().getAddress(), peeraddr.inetAddress());

            assertEquals(peeraddr.peerId(), peerId);

            // multiaddress is just a fiction
            Lite.dial(session, peeraddr, Lite.connectionParameters(ALPN.libp2p));

        }
    }


    @Test
    public void test_ipv6() throws Exception {

        String a1 = "/ip6/2804:d41:432f:3f00:ccbd:8e0d:a023:376b/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a2 = "/ip6/2804:d41:432f:3f00:94b9:ba41:dfc7:af66/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a3 = "/ip6/2804:d41:432f:3f00:a5b1:7947:debe:c88b/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";
        String a4 = "/ip6/2804:d41:432f:3f00:250e:c623:3b25:ddc8/udp/4001/quic-v1/p2p/12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU";

        Peeraddr peeraddr1 = Peeraddr.create(a1);
        assertNotNull(peeraddr1);
        Peeraddr peeraddr2 = Peeraddr.create(a2);
        assertNotNull(peeraddr2);
        Peeraddr peeraddr3 = Peeraddr.create(a3);
        assertNotNull(peeraddr3);
        Peeraddr peeraddr4 = Peeraddr.create(a4);
        assertNotNull(peeraddr4);
    }

}
