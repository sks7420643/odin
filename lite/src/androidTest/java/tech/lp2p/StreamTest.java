package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;

public class StreamTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_string() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            String text = "Hello Moin und Zehn Elf";
            Raw raw = Lite.storeText(session, text);
            assertNotNull(raw);

            byte[] result = Lite.fetchData(session, raw.cid());
            assertNotNull(result);
            assertEquals(text, new String(result));

            Raw raw1 = Lite.storeText(session, "TEST test");
            assertNotNull(raw1);
        }

    }
}
