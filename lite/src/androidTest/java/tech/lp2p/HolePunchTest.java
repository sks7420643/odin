package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.utils.Utils;


public class HolePunchTest {
    private static final String TAG = HolePunchTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_holepunch_connect() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);

        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!server.hasPublicPeeraddrs()) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        assertTrue(Lite.hasReservations(server));
        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {

            Set<Reservation> reservations = Lite.reservations(server);
            assertFalse(reservations.isEmpty());
            Reservation reservation = reservations.stream().
                    findAny().orElseThrow((Supplier<Throwable>) () ->
                            new RuntimeException("at least one present"));


            PeerId relayID = reservation.peerId();
            assertNotNull(relayID);
            Peeraddr relay = reservation.peeraddr();
            assertNotNull(relay);

            assertTrue(reservation.limitData() > 0);
            assertTrue(reservation.limitDuration() > 0);

            Connection connection = Lite.dial(dummySession, lite.self(),
                    Lite.connectionParameters(ALPN.lite), new TimeoutCancellable(120));
            Objects.requireNonNull(connection);


            Cid test1 = Lite.rawCid("moin elf");
            Envelope data = Lite.createEnvelope(dummySession, TestEnv.RAW, test1);


            Lite.pushEnvelope(dummySession, connection, data);
            Thread.sleep(2000);

            try {
                Envelope envelope = TestEnv.PUSHED.get();
                assertArrayEquals(envelope.cid().encoded(), test1.encoded());
            } catch (Exception e) {
                fail();
            }

            Cid test2 = Lite.rawCid("moin zehn");
            Envelope data2 = Lite.createEnvelope(dummySession, TestEnv.RAW, test2);


            Lite.pushEnvelope(dummySession, connection, data2);
            Thread.sleep(2000);
            try {
                Envelope envelope = TestEnv.PUSHED.get();
                assertArrayEquals(envelope.cid().encoded(), test2.encoded());
            } catch (Exception e) {
                fail();
            }

            // will close the relay connection from the dummy
            connection.close();

            assertTrue(Lite.hasReservations(server));

        } finally {
            dummy.clearDatabase();
        }
    }

}
