package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class EnvelopePullTest {


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void failure_test() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        assertEquals(server.numConnections(), 0);
        Dummy dummy = Dummy.getInstance(context);

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        try (Session dummySession = dummy.createSession()) {

            Parameters parameters = Parameters.create(ALPN.lite, false);
            Connection connection = Lite.dial(dummySession, peeraddr, parameters);
            Objects.requireNonNull(connection);

            Thread.sleep(1000);

            assertEquals(server.numConnections(), 1);


            // simple failure test
            Payload failure = new Payload("failure", 25);
            Envelope envelope = Lite.pullEnvelope(dummySession, connection, failure);
            assertNull(envelope);

            connection.close();
        }
        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);
    }
}
