package tech.lp2p;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;
import tech.lp2p.utils.Utils;


public class CatTest {

    private static final String TAG = CatTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void providers_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            Cid cid = Lite.decodeCid("Qmaisz6NMhDB51cCvNWa1GMS7LU1pAxdF4Ld6Ft9kZEP2a");

            long time = System.currentTimeMillis();

            Set<Peeraddr> provs = ConcurrentHashMap.newKeySet();
            Key key = Lite.createKey(cid);

            Lite.providers(session, key, provs::add,
                    new TimeoutCancellable(() -> provs.size() > 10, 60));

            assertFalse(provs.isEmpty());

            for (Peeraddr prov : provs) {
                Utils.info(TAG, "Provider " + prov);
            }
            Utils.debug(TAG, "Time Providers : " + (System.currentTimeMillis() - time) + " [ms]");
        }
    }


    @Test
    public void cat_not_exist() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            Cid cid = Lite.decodeCid("QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nt");
            try {
                Lite.fetchData(session, cid);
                fail();
            } catch (Exception ignore) {
                //
            }
        }
    }


    @Test
    public void cat_test_local() throws Exception {


        Lite lite = TestEnv.getTestInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            Raw local = Lite.storeText(session, "Moin Moin Moin");
            assertNotNull(local);

            byte[] content = Lite.fetchData(session, local.cid());

            assertNotNull(content);
        }
    }

}