package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;


public class DatagramTest {


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void datagram_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Dummy dummy = Dummy.getInstance(context);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());


        try (Session dummySession = dummy.createSession()) {
            Parameters parameters = Parameters.create(ALPN.lite, false);
            Connection conn = Lite.dial(dummySession, peeraddr, parameters);
            Objects.requireNonNull(conn);

            Thread.sleep(1000);

            assertEquals(server.numConnections(), 1);

            String text = "Moin Moin";


            // now send a datagram [no way yet implemented to check if the
            // datagram has arrived on the server, just an error log LogUtils.error,
            // since no application is defined to handle it]
            ((tech.lp2p.quic.Connection) conn).sendDatagram(text.getBytes());

            conn.close();

            Thread.sleep(1000);
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);


    }

}
