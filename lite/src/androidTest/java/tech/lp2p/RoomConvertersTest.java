package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Peeraddr;
import tech.lp2p.store.RoomConverters;

public class RoomConvertersTest {
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void peer_store_test() throws Exception {

        int port = 4001;

        Lite lite = TestEnv.getTestInstance(context);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), port);
        assertNotNull(peeraddr);
        byte[] data = RoomConverters.toArray(peeraddr);
        assertNotNull(data);
        Peeraddr cmp = RoomConverters.fromArrayToPeeraddr(data);
        assertNotNull(cmp);
        assertEquals(peeraddr, cmp);
    }
}
