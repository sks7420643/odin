package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.utils.Utils;

public class HolepunchTest {

    private static final String TAG = HolepunchTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_holepunch() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);

        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (!server.hasPublicPeeraddrs()) {
            Utils.info(TAG, "nothing to test no dialable addresses");
            return;
        }

        assertTrue(Lite.hasReservations(server));

        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {

            Set<Reservation> reservations = Lite.reservations(server);
            assertFalse(reservations.isEmpty());

            Connection connection = Lite.dial(dummySession, lite.self(),
                    Lite.connectionParameters(ALPN.libp2p),
                    new TimeoutCancellable(120));
            assertNotNull(connection);

            Identify info = Lite.identify(connection);
            assertNotNull(info);
            Utils.error(TAG, info.toString());

            connection.close();

        }
        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);
    }

}