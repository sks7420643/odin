package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.core.TimeoutCancellable;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;
import tech.lp2p.utils.Utils;


public class RelaysTest {
    private static final String TAG = RelaysTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_connect_relays() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);

        if (!TestEnv.isNetworkConnected(context)) {
            Utils.info(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Server server = TestEnv.getServer();
        assertNotNull(server);

        String text = new String(TestEnv.getRandomBytes(10000));

        Raw cid;
        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            cid = Lite.storeText(session, text);
        }
        assertNotNull(cid);


        Set<Reservation> reservations = server.reservations();
        assertNotNull(reservations);
        assertFalse(reservations.isEmpty());


        Reservation reservation = reservations.iterator().next();
        assertNotNull(reservation);


        // now dial the multiaddr from a dummy
        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {


            Connection connection = Lite.dial(dummySession, lite.self(),
                    Lite.connectionParameters(ALPN.libp2p),
                    new TimeoutCancellable(120));
            assertNotNull(connection);


            Peeraddr remoteAddress = connection.remotePeeraddr();
            Utils.error(TAG, "Remote connection " + remoteAddress);

            // TEST 1 (Info about node)
            Identify identify = Lite.identify(connection);
            assertNotNull(identify);
            Utils.error(TAG, identify.toString());


            // 1.2 (check agent and version)
            assertEquals(identify.agent(), TestEnv.AGENT);

            // 1.3 (check peerId) -> it is ipfs.self()
            assertEquals(identify.peerId(), lite.self());

            // 1.4 (check protocols) -> it is ipfs server protocols
            List<String> protocols = Arrays.asList(identify.protocols());

            assertTrue(protocols.contains(Protocol.MULTISTREAM_PROTOCOL.name()));
            assertTrue(protocols.contains(Protocol.IDENTITY_PROTOCOL.name()));
            assertTrue(protocols.contains(Protocol.IDENTITY_PUSH_PROTOCOL.name()));
            assertTrue(protocols.contains(Protocol.DHT_PROTOCOL.name()));
            assertTrue(protocols.contains(Protocol.RELAY_PROTOCOL_HOP.name()));
            assertEquals(protocols.size(), 5);

        }
    }

}
