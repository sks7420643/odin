package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Objects;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;
import tech.lp2p.utils.Utils;

public class ClientServerTest {


    private static final String TAG = ClientServerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void server_stress_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numConnections(), 0);
        Dummy dummy = Dummy.getInstance(context);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            PeerId host = lite.self();
            assertNotNull(host);
            Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

            byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

            Fid cid = TestEnv.createContent(session, "random.bin", input);
            assertNotNull(cid);


            try (Session dummySession = dummy.createSession()) {

                Parameters parameters = Parameters.create(ALPN.lite, false);
                Connection connection = Lite.dial(dummySession, peeraddr, parameters);
                Objects.requireNonNull(connection);

                Thread.sleep(1000);

                assertEquals(server.numConnections(), 1);


                //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
                byte[] output = Lite.fetchData(connection, cid.cid(), new Progress() {
                    @Override
                    public void setProgress(int progress) {
                        Utils.info(TAG, String.valueOf(progress));
                    }

                    @Override
                    public boolean isCancelled() {
                        return false;
                    }
                });
                assertArrayEquals(input, output);

                connection.close();

                Thread.sleep(1000);

            } finally {
                dummy.clearDatabase();
            }

            Thread.sleep(3000);
            assertEquals(server.numConnections(), 0);
        }

    }


    @Test
    public void big_server_stress_test() throws Exception {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        try (Session session = lite.createSession(BLOCKS.getInstance(context), PEERS.getInstance(context))) {
            Dummy dummy = Dummy.getInstance(context);

            PeerId host = lite.self();
            assertNotNull(host);
            Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

            int packetSize = 1000;
            long maxData = 100000;
            File inputFile = TestEnv.createCacheFile(context);
            try (OutputStream outputStream = new FileOutputStream(inputFile)) {
                for (int i = 0; i < maxData; i++) {
                    byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                    outputStream.write(randomBytes);
                }
            }
            Fid cid = Lite.storeFile(session, inputFile);
            assertNotNull(cid);
            try (Session dummySession = dummy.createSession()) {

                Parameters parameters = Parameters.create(ALPN.lite, false);
                Connection connection = Lite.dial(dummySession, peeraddr, parameters);
                Objects.requireNonNull(connection);

                Thread.sleep(1000);

                assertEquals(server.numConnections(), 1);


                File temp = TestEnv.createCacheFile(context);
                Lite.fetchToFile(connection, temp, cid.cid(),
                        new TestEnv.DummyProgress());

                TestCase.assertEquals(temp.length(), inputFile.length());

                assertTrue(temp.delete());

                connection.close();

                Thread.sleep(1000);

            } finally {
                dummy.clearDatabase();
            }

            Thread.sleep(3000);
            assertEquals(server.numConnections(), 0);
        }

    }
}
