package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Reservations;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;

public class RelayTest {
    private static final String TAG = HolePunchTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test(expected = Exception.class)
    public void permissionDeniedReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        try {
            Lite.reservation(server, relayPeeraddr); // exception expected
            fail();
        } finally {
            Thread.sleep(3000); // necessary so that the server is able to remove connection
            assertEquals(server.numConnections(), 0);
        }
    }


    @Test(expected = Exception.class)
    public void permissionDeniedConnection() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        try (Session session = lite.createSession(
                new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore())) {

            Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
            Lite.connection(session, relayPeeraddr, lite.self(), parameters); // exception expected
            fail();
        } finally {
            Thread.sleep(3000); // necessary so that the server is able to remove connection
            assertEquals(server.numConnections(), 0);
        }

    }

    @Test
    public void positiveReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        Utils.error(TAG, "Relay Server  " + relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });
        Utils.error(TAG, "Alice Server Port " + alicePort);
        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());


            Lite bob = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                    new Peeraddrs(), TestEnv.AGENT, new Payloads()));

            try (Session bobSession = bob.createSession(
                    new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore())) {

                Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
                Connection bobToAlice = Lite.connection(bobSession, relayPeeraddr,
                        alice.self(), parameters);
                assertNotNull(bobToAlice);

                Identify identify = Lite.identify(bobToAlice);
                assertNotNull(identify);
                assertEquals(identify.peerId(), alice.self());
                assertEquals(identify.peerId(), bobToAlice.remotePeerId());

                assertTrue(bobToAlice.isConnected());
                bobToAlice.close();

            }

            Reservations reservations = Lite.reservations(server);
            assertTrue(reservations.isEmpty()); // should be empty

        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);

    }


    @Test
    public void reservationRefusedTest() throws Throwable {


        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        Utils.error(TAG, "Relay Server  " + relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });
        Utils.error(TAG, "Alice Server Port " + alicePort);
        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(server);
            assertEquals(reservations.size(), 1);

            Thread.sleep(3000);
            boolean failed = false;
            try {
                Lite.reservation(aliceServer, relayPeeraddr);
            } catch (Throwable expected) {
                failed = true;
                Utils.error(TAG, expected.getMessage());
            }
            assertTrue(failed);

            reservations = Lite.reservations(server);
            assertEquals(reservations.size(), 1);
            assertEquals(reservations.iterator().next(), reservation);
        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);

    }


    @Test
    public void updateReservationTest() throws Throwable {


        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        Utils.error(TAG, "Relay Server  " + relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeyPair(),
                new Peeraddrs(), TestEnv.AGENT, new Payloads()));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> Utils.error(TAG, "Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> Utils.error(TAG, "Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> Utils.error(TAG, "Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> Utils.error(TAG, "Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });
        Utils.error(TAG, "Alice Server Port " + alicePort);
        try {

            Reservation reservation = Lite.reservation(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Reservations reservations = Lite.reservations(server);
            assertEquals(reservations.size(), 1);

            Thread.sleep(3000);
            Reservation updated = Lite.updateReservation(server, reservation);
            assertNotEquals(reservation, updated);


            reservations = Lite.reservations(server);
            assertEquals(reservations.size(), 1);
            assertEquals(reservations.iterator().next(), updated);

        } finally {
            aliceServer.shutdown();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);

    }

}
