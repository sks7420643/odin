package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.ExecutionException;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class DhtTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test(expected = ExecutionException.class)
    public void providers() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);
        Dummy dummy = Dummy.getInstance(context);
        Connection connection = null;
        try (Session dummySession = dummy.createSession()) {

            PeerId host = lite.self();
            assertNotNull(host);
            Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

            Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
            connection = Lite.dial(dummySession, peeraddr, parameters);

            Cid cid = Lite.rawCid("test");
            Key key = Lite.createKey(cid);

            // should throw an exception (server does not support GET_PROVIDERS)
            Peeraddrs providers = Lite.providers(connection, key);
            assertNull(providers);

            fail();

        } finally {
            if (connection != null) {
                connection.close();

                Thread.sleep(3000);
                assertEquals(server.numConnections(), 0);
            }
        }
    }

    @Test
    public void findPeer() throws Throwable {

        Lite lite = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        assertEquals(server.numConnections(), 0);
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {

            PeerId host = lite.self();
            assertNotNull(host);
            Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

            Parameters parameters = Lite.connectionParameters(ALPN.libp2p);
            Connection connection = Lite.dial(dummySession, peeraddr, parameters);
            PeerId peerId = TestEnv.random();

            Peeraddrs peeraddrs = Lite.findPeer(connection, peerId);
            if (TestEnv.isNetworkConnected(context)) {
                assertFalse(peeraddrs.isEmpty());
            } else {
                assertTrue(peeraddrs.isEmpty());
            }

            connection.close();
        }

        Thread.sleep(3000);
        assertEquals(server.numConnections(), 0);
    }
}
