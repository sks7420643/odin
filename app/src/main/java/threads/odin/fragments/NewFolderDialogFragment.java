package threads.odin.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.core.DOCS;

public class NewFolderDialogFragment extends DialogFragment {
    public static final String TAG = NewFolderDialogFragment.class.getSimpleName();


    private final AtomicBoolean notPrintErrorMessages = new AtomicBoolean(false);
    private long mLastClickTime = 0;
    private TextInputEditText mNewFolder;
    private TextInputLayout mFolderLayout;

    public static NewFolderDialogFragment newInstance(long parent) {
        Bundle bundle = new Bundle();
        bundle.putLong(DOCS.IDX, parent);

        NewFolderDialogFragment fragment = new NewFolderDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        notPrintErrorMessages.set(true);
        isValidFolderName(getDialog());
        notPrintErrorMessages.set(false);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getLayoutInflater();
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        long parent = args.getLong(DOCS.IDX, 0);

        View view = inflater.inflate(R.layout.new_folder, null);
        mFolderLayout = view.findViewById(R.id.folder_layout);
        mFolderLayout.setCounterEnabled(true);
        mFolderLayout.setCounterMaxLength(50);

        mNewFolder = view.findViewById(R.id.new_folder_text);
        InputFilter[] filterTitle = new InputFilter[1];
        filterTitle[0] = new InputFilter.LengthFilter(50);
        mNewFolder.setFilters(filterTitle);
        mNewFolder.requestFocus();

        mNewFolder.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isValidFolderName(getDialog());
            }
        });


        builder.setView(view)
                // Add action buttons
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < DOCS.CLICK_OFFSET) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    removeKeyboards();


                    Editable text = mNewFolder.getText();
                    Objects.requireNonNull(text);
                    String name = text.toString();

                    ExecutorService executor = Executors.newSingleThreadExecutor();
                    executor.execute(() -> {
                        try {
                            DOCS docs = DOCS.getInstance(requireContext());
                            docs.createDirectory(parent, name);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    });
                })

                .setTitle(R.string.new_folder);


        return builder.create();
    }

    private void isValidFolderName(Dialog dialog) {
        if (dialog instanceof AlertDialog alertDialog) {
            Editable text = mNewFolder.getText();
            Objects.requireNonNull(text);
            String multi = text.toString();


            boolean result = !multi.isEmpty();


            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(result);


            if (!notPrintErrorMessages.get()) {

                if (multi.isEmpty()) {
                    mFolderLayout.setError(getString(R.string.name_not_valid));
                } else {
                    mFolderLayout.setError(null);
                }

            } else {
                mFolderLayout.setError(null);
            }
        }
    }

    private void removeKeyboards() {

        try {
            InputMethodManager imm = (InputMethodManager)
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mNewFolder.getWindowToken(), 0);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        removeKeyboards();
    }

}
