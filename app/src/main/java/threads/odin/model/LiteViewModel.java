package threads.odin.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.List;
import java.util.Objects;

import threads.odin.core.conns.CONNS;
import threads.odin.core.conns.Conn;
import threads.odin.core.conns.ConnsDatabase;
import threads.odin.core.files.FILES;
import threads.odin.core.files.FileInfo;
import threads.odin.core.files.FileInfoDatabase;
import threads.odin.core.relays.RELAYS;
import threads.odin.core.relays.Relay;
import threads.odin.core.relays.RelaysDatabase;

public class LiteViewModel extends AndroidViewModel {


    @NonNull
    private final MutableLiveData<Boolean> showFab = new MutableLiveData<>(true);

    @NonNull
    private final MutableLiveData<Long> parentFile = new MutableLiveData<>(0L);
    @NonNull
    private final MediatorLiveData<Void> liveDataMerger = new MediatorLiveData<>();

    @NonNull
    private final FileInfoDatabase fileInfoDatabase;

    @NonNull
    private final ConnsDatabase connsDatabase;

    @NonNull
    private final RelaysDatabase relaysDatabase;

    public LiteViewModel(@NonNull Application application) {
        super(application);

        fileInfoDatabase = FILES.getInstance(application.getApplicationContext()).database();
        connsDatabase = CONNS.getInstance(application.getApplicationContext()).database();
        relaysDatabase = RELAYS.getInstance(application.getApplicationContext()).database();
        liveDataMerger.addSource(parentFile, value -> liveDataMerger.setValue(null));

    }


    @NonNull
    public MutableLiveData<Long> getParentFileIdx() {
        return parentFile;
    }

    public void setParentFileIdx(long idx) {
        parentFile.postValue(idx);
    }

    @NonNull
    public LiveData<List<FileInfo>> liveDataFiles() {
        return Transformations.switchMap(
                liveDataMerger, input -> fileInfoDatabase.fileInfoDao().getLiveDataFiles(
                        getParentFileIdxValue()));
    }

    @NonNull
    public LiveData<List<Conn>> liveDataConns() {
        return connsDatabase.connDao().conns();
    }

    @NonNull
    public LiveData<List<Relay>> liveDataRelays() {
        return relaysDatabase.relayDao().relays();
    }


    public long getParentFileIdxValue() {
        Long value = parentFile.getValue();
        return Objects.requireNonNull(value); // this should never be null
    }


    @NonNull
    public MutableLiveData<Boolean> getShowFab() {
        return showFab;
    }

    public void setShowFab(boolean show) {
        showFab.postValue(show);
    }
}