package threads.odin;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import tech.lp2p.core.Server;
import threads.odin.core.DOCS;
import threads.odin.core.events.EVENTS;
import threads.odin.core.mdns.MDNS;

public class InitApplication extends Application {


    private static final String APP_KEY = "LITE_KEY";
    private static final String TITLE_KEY = "TITLE_KEY";
    private static final String TAG = InitApplication.class.getSimpleName();

    private MDNS mdns;

    public static void setTitle(@NonNull Context context, @NonNull String title) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TITLE_KEY, title);
        editor.apply();
    }

    @NonNull
    public static String getTitle(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(TITLE_KEY,
                context.getString(R.string.homepage)));

    }


    @Override
    public void onCreate() {
        super.onCreate();

        DynamicColors.applyToActivitiesIfAvailable(this);

        registerService();
    }


    private void registerService() {
        EVENTS events = EVENTS.getInstance(getApplicationContext());
        try {

            mdns = MDNS.mdns(getApplicationContext());

            DOCS docs = DOCS.getInstance(getApplicationContext());
            Server server = docs.getServer();
            mdns.startService(server);

            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> docs.initPinsPage(getApplicationContext()));
        } catch (Throwable throwable) {
            events.fatal(throwable.getClass().getSimpleName() + throwable.getMessage());
            unRegisterService();
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unRegisterService();
    }

    private void unRegisterService() {
        try {
            if (mdns != null) {
                mdns.stop();
                mdns = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
