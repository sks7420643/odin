package threads.odin.utils;

@SuppressWarnings("WeakerAccess")
public interface FileItemPosition {
    int getPosition(long idx);
}
