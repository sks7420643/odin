package threads.odin.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.core.relays.Relay;

public class RelaysAdapter extends RecyclerView.Adapter<RelaysAdapter.ViewHolder> {
    private static final String TAG = RelaysAdapter.class.getSimpleName();
    private final List<Relay> relays = new ArrayList<>();

    public RelaysAdapter() {
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.peeraddr;
    }

    @Override
    @NonNull
    public RelaysAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.onBind(relays.get(position));
    }


    @Override
    public int getItemCount() {
        return relays.size();
    }

    public void updateData(@NonNull List<Relay> relays) {
        final RelayDiffCallback diffCallback = new RelayDiffCallback(this.relays, relays);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.relays.clear();
        this.relays.addAll(relays);
        diffResult.dispatchUpdatesTo(this);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView address;
        final TextView title;
        final ImageView image;

        private ViewHolder(View v) {
            super(v);

            this.title = itemView.findViewById(R.id.peerid);
            this.address = itemView.findViewById(R.id.address);
            this.image = itemView.findViewById(R.id.image);
        }

        void onBind(@NonNull Relay relay) {

            try {
                String title = relay.peeraddr().peerId().toString();
                this.title.setText(title);
                String address = relay.peeraddr().inetAddress().toString() + ":" +
                        relay.peeraddr().port();
                this.address.setText(address);
                this.image.setImageResource(R.drawable.server_network);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }
}
