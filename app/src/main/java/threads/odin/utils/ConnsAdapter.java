package threads.odin.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import tech.lp2p.core.Peeraddr;
import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.core.conns.Conn;

public class ConnsAdapter extends RecyclerView.Adapter<ConnsAdapter.ViewHolder> {
    private static final String TAG = ConnsAdapter.class.getSimpleName();

    private final List<Conn> conns = new ArrayList<>();

    public ConnsAdapter() {
    }


    public void updateData(@NonNull List<Conn> conns) {
        final ConnDiffCallback diffCallback = new ConnDiffCallback(this.conns, conns);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.conns.clear();
        this.conns.addAll(conns);
        diffResult.dispatchUpdatesTo(this);
    }


    @Override
    public int getItemViewType(int position) {
        return R.layout.peeraddr;
    }

    @Override
    @NonNull
    public ConnsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Conn conn = conns.get(position);
        holder.onBind(conn.peeraddr());
    }


    @Override
    public int getItemCount() {
        return conns.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView address;
        final TextView title;
        final ImageView image;

        private ViewHolder(View v) {
            super(v);

            this.title = itemView.findViewById(R.id.peerid);
            this.address = itemView.findViewById(R.id.address);
            this.image = itemView.findViewById(R.id.image);
        }


        void onBind(@NonNull Peeraddr peeraddr) {

            try {
                String title = peeraddr.peerId().toString();
                this.title.setText(title);
                String address = peeraddr.inetAddress().toString() + ":" + peeraddr.port();
                this.address.setText(address);
                this.image.setImageResource(R.drawable.server_network);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

    }
}
