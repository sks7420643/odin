package threads.odin.utils;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

import threads.odin.core.conns.Conn;

@SuppressWarnings("WeakerAccess")
public class ConnDiffCallback extends DiffUtil.Callback {
    private final List<Conn> mOldList;
    private final List<Conn> mNewList;

    public ConnDiffCallback(List<Conn> oldConns, List<Conn> newConns) {
        this.mOldList = oldConns;
        this.mNewList = newConns;
    }

    public static boolean areItemsTheSame(Conn oldConn, Conn newConn) {
        return Objects.equals(oldConn.peeraddr(), newConn.peeraddr());
    }

    public static boolean areContentsTheSame(Conn oldConn, Conn newConn) {
        return Objects.equals(oldConn.peeraddr(), newConn.peeraddr());
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return areContentsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }
}
