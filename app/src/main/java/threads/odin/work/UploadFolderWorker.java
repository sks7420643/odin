package threads.odin.work;


import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;
import threads.odin.LogUtils;
import threads.odin.core.DOCS;
import threads.odin.core.files.FILES;


public final class UploadFolderWorker extends Worker {
    private static final String TAG = UploadFolderWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFolderWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long idx, @NonNull Uri uri) {

        Data.Builder data = new Data.Builder();
        data.putString(DOCS.URI, uri.toString());
        data.putLong(DOCS.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFolderWorker.class)
                .addTag(UploadFolderWorker.class.getSimpleName())
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, long idx, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                UploadFolderWorker.class.getSimpleName(),
                ExistingWorkPolicy.APPEND, getWork(idx, uri));

    }


    @NonNull
    @Override
    public Result doWork() {

        String uri = getInputData().getString(DOCS.URI);
        Objects.requireNonNull(uri);
        long root = getInputData().getLong(DOCS.IDX, 0L);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);


        try {

            FILES files = FILES.getInstance(getApplicationContext());
            BLOCKS blocks = BLOCKS.getInstance(getApplicationContext());
            PEERS peers = PEERS.getInstance(getApplicationContext());
            DocumentFile rootDocFile = DocumentFile.fromTreeUri(getApplicationContext(),
                    Uri.parse(uri));
            Objects.requireNonNull(rootDocFile);


            String name = rootDocFile.getName();
            Objects.requireNonNull(name);


            DOCS docs = DOCS.getInstance(getApplicationContext());
            long parent = docs.createDirectory(root, name);
            try (Session session = docs.lite().createSession(blocks, peers)) {

                files.setWork(parent, getId());
                files.setUri(parent, uri);

                copyDir(session, parent, rootDocFile);

            } finally {
                files.setDone(parent);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private void copyDir(Session session, long parent, DocumentFile file) throws Exception {
        DOCS docs = DOCS.getInstance(getApplicationContext());
        FILES files = FILES.getInstance(getApplicationContext());
        DocumentFile[] filesInDir = file.listFiles();

        for (DocumentFile docFile : filesInDir) {
            if (!isStopped()) {
                if (docFile.isDirectory()) {
                    String name = docFile.getName();
                    Objects.requireNonNull(name);
                    long child = docs.createDirectory(parent, name);
                    copyDir(session, child, docFile);
                    files.setDone(child);
                    docs.finishDocument(child);
                } else {
                    long child = copyFile(session, parent, docFile
                    );
                    docs.finishDocument(child);
                }
            }
        }
    }

    private long copyFile(Session session, long parent, DocumentFile file) throws Exception {

        if (isStopped()) {
            return 0L;
        }

        FILES files = FILES.getInstance(getApplicationContext());


        long idx = createDocument(parent, file);
        long size = file.length();
        String name = file.getName();
        Objects.requireNonNull(name);
        files.setWork(idx, getId());
        Uri uri = file.getUri();

        try (InputStream inputStream = getApplicationContext().getContentResolver().
                openInputStream(uri)) {
            Objects.requireNonNull(inputStream);

            Fid fid = Lite.storeInputStream(session, name, inputStream, new Progress() {

                @Override
                public void setProgress(int progress) {
                }

                @Override
                public boolean isCancelled() {
                    return isStopped();
                }

            }, size);


            files.setDone(idx, fid.cid());
            return idx;

        } catch (Throwable throwable) {
            files.finalDeleting(idx);
        }

        return 0L;
    }

    private long createDocument(long parent, @NonNull DocumentFile file) throws Exception {

        Uri uri = file.getUri();
        long size = file.length();
        String name = file.getName();
        Objects.requireNonNull(name);
        String mimeType = file.getType();
        DOCS docs = DOCS.getInstance(getApplicationContext());

        return docs.createDocument(parent, mimeType, null, uri, name, size);
    }

}
