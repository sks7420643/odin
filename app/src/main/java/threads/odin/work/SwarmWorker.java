package threads.odin.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import tech.lp2p.Lite;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.TimeoutCancellable;
import threads.odin.LogUtils;
import threads.odin.core.DOCS;

public final class SwarmWorker extends Worker {
    private static final String TAG = SwarmWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public SwarmWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long minutes) {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        return new OneTimeWorkRequest.Builder(SwarmWorker.class)
                .addTag(TAG)
                .setInitialDelay(minutes, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build();
    }

    public static void reservations(@NonNull Context context, ExistingWorkPolicy policy, long minutes) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, policy, getWork(minutes));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, "Worker Start " + getId() + " ...");

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            Server server = docs.getServer();
            Objects.requireNonNull(server);


            Set<Reservation> reservations = Lite.reservations(
                    server, new TimeoutCancellable(this::isStopped, 180));

            for (Reservation reservation : reservations) {
                LogUtils.error(TAG, reservation.toString());
            }

            long minutes = Lite.nextReservationCycle(server);
            if (minutes == 0) {
                minutes = 15;
            }

            SwarmWorker.reservations(getApplicationContext(),
                    ExistingWorkPolicy.APPEND, minutes);

            for (Peeraddr peeraddr : Lite.reservationPeeraddrs(server)) {
                LogUtils.error(TAG, "Dialable Address " + peeraddr.toString());
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "Worker Finish " + getId() +
                    " onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }
}
