package threads.odin.work;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;
import threads.odin.LogUtils;
import threads.odin.core.DOCS;
import threads.odin.core.files.FILES;
import threads.odin.core.files.FileInfo;

public final class UploadFileWorker extends Worker {
    private static final String TAG = UploadFileWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(DOCS.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFileWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, long idx) {
        WorkManager.getInstance(context).enqueue(getWork(idx));
    }


    @NonNull
    @Override
    public Result doWork() {


        long idx = getInputData().getLong(DOCS.IDX, -1);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);


        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());
            BLOCKS blocks = BLOCKS.getInstance(getApplicationContext());
            PEERS peers = PEERS.getInstance(getApplicationContext());

            files.setWork(idx, getId());

            FileInfo fileInfo = files.getFileInfo(idx);
            Objects.requireNonNull(fileInfo);

            String url = fileInfo.uri();
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);

            long size = fileInfo.size();

            // normal case like content of files

            try (Session session = docs.lite().createSession(blocks, peers)) {
                try (InputStream inputStream = getApplicationContext().getContentResolver()
                        .openInputStream(uri)) {
                    Objects.requireNonNull(inputStream);

                    Fid fid = Lite.storeInputStream(session, fileInfo.name(), inputStream, new Progress() {
                        @Override
                        public void setProgress(int progress) {

                        }

                        @Override
                        public boolean isCancelled() {
                            return isStopped();
                        }
                    }, size);

                    Objects.requireNonNull(fid);
                    files.setDone(idx, fid.cid());
                    docs.finishDocument(idx);

                } catch (Throwable throwable) {
                    files.finalDeleting(idx);
                    throw throwable;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

}
