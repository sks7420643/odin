package threads.odin;


import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;
import androidx.work.ExistingWorkPolicy;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.search.SearchBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.odin.core.DOCS;
import threads.odin.core.events.EVENTS;
import threads.odin.core.files.FILES;
import threads.odin.core.files.FileInfo;
import threads.odin.fragments.FilesFragment;
import threads.odin.fragments.InfoSheetDialogFragment;
import threads.odin.fragments.NewFolderDialogFragment;
import threads.odin.fragments.NewTitleDialogFragment;
import threads.odin.fragments.TextDialogFragment;
import threads.odin.model.EventViewModel;
import threads.odin.model.LiteViewModel;
import threads.odin.utils.MimeTypeService;
import threads.odin.work.ResetWorker;
import threads.odin.work.SwarmWorker;
import threads.odin.work.UploadFolderWorker;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private long lastClickTime = 0;
    private LinearLayout mainLayout;
    private LiteViewModel liteViewModel;
    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (DOCS.isPartial(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(getApplicationContext(),
                                                    liteViewModel.getParentFileIdxValue(), uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!DOCS.hasReadPermission(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (DOCS.isPartial(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(getApplicationContext(),
                                                liteViewModel.getParentFileIdxValue(), uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private FilesFragment filesFragment;
    private boolean widthMode = false;


    private void evaluateDisplayModes() {
        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(this);

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;
        widthMode = widthDp >= 600;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            evaluateDisplayModes();
            invalidateOptionsMenu();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        MenuCompat.setGroupDividerEnabled(menu, true);

        menu.findItem(R.id.action_new_folder).setVisible(!widthMode);
        menu.findItem(R.id.action_import_folder).setVisible(!widthMode);
        menu.findItem(R.id.action_import_folder).setVisible(!widthMode);
        menu.findItem(R.id.action_new_text).setVisible(!widthMode);
        menu.findItem(R.id.action_settings).setVisible(!widthMode);


        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        networkCallback();
    }

    public void networkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);

            ConnectivityManager.NetworkCallback networkCallback =
                    new MainNetworkCallback(getApplicationContext());

            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .build();

            connectivityManager.registerNetworkCallback(networkRequest, networkCallback);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (item.getItemId() == android.R.id.home) {

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                if (liteViewModel.getParentFileIdxValue() != 0L) {
                    liteViewModel.setParentFileIdx(0L);
                }
                return true;
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        } else if (id == R.id.action_settings) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            NewTitleDialogFragment.newInstance().
                    show(getSupportFragmentManager(), NewTitleDialogFragment.TAG);

            return true;
        } else if (item.getItemId() == R.id.action_new_folder) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            NewFolderDialogFragment.newInstance(
                            liteViewModel.getParentFileIdxValue()).
                    show(getSupportFragmentManager(), NewFolderDialogFragment.TAG);

            return true;
        } else if (item.getItemId() == R.id.action_import_folder) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                mFolderImportForResult.launch(intent);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            return true;
        } else if (item.getItemId() == R.id.action_new_text) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            TextDialogFragment.newInstance(liteViewModel.getParentFileIdxValue()).
                    show(getSupportFragmentManager(), TextDialogFragment.TAG);

            return true;
        } else if (item.getItemId() == R.id.action_share) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                DOCS docs = DOCS.getInstance(getApplicationContext());
                String link = docs.getHomePageUri().toString();

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
                intent.putExtra(Intent.EXTRA_TEXT, link);
                intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(chooser);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            return true;
        } else if (item.getItemId() == R.id.action_documentation) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://gitlab.com/lp2p/odin"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } catch (Throwable e) {
                EVENTS.getInstance(getApplicationContext()).warning(
                        getString(R.string.no_activity_found_to_handle_uri));
            }

            return true;
        } else if (item.getItemId() == R.id.action_reset) {

            if (SystemClock.elapsedRealtime() - lastClickTime < DOCS.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(MainActivity.this);
                alertDialog.setTitle(getString(R.string.warning));
                alertDialog.setMessage(getString(R.string.reset_application_data));
                alertDialog.setPositiveButton(getString(android.R.string.ok),
                        (dialogInterface, which) -> {
                            ResetWorker.reset(getApplicationContext());
                            liteViewModel.setParentFileIdx(0L);
                            dialogInterface.dismiss();
                        });
                alertDialog.setNeutralButton(getString(android.R.string.cancel),
                        (dialogInterface, which) -> dialogInterface.dismiss());
                alertDialog.show();

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getOnBackPressedDispatcher().addCallback(this, new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                boolean result = filesFragment.onBackPressed();
                if (result) {
                    return;
                }
                finish();
            }
        });

        evaluateDisplayModes();

        filesFragment = (FilesFragment) getSupportFragmentManager()
                .findFragmentById(R.id.files_container);

        SearchBar searchBar = findViewById(R.id.search_bar);
        setSupportActionBar(searchBar);
        searchBar.setText(InitApplication.getTitle(getApplicationContext()));
        searchBar.setOnClickListener(v -> {
            try {
                DOCS docs = DOCS.getInstance(getApplicationContext());
                Uri uri = docs.getHomePageUri();

                InfoSheetDialogFragment.newInstance(uri)
                        .show(getSupportFragmentManager(), InfoSheetDialogFragment.TAG);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mainLayout = findViewById(R.id.main_layout);

        TextView reachability = findViewById(R.id.reachability);

        liteViewModel = new ViewModelProvider(this).get(LiteViewModel.class);
        liteViewModel.getParentFileIdx().observe(this, (parent) -> {
            if (parent != null) {
                if (parent == 0L) {
                    searchBar.setText(InitApplication.getTitle(getApplicationContext()));
                } else {
                    FILES files = FILES.getInstance(getApplicationContext());
                    FileInfo fileInfo = files.getFileInfo(parent);
                    if (fileInfo != null) {
                        searchBar.setText(fileInfo.name());
                    } else {
                        searchBar.setText(InitApplication.getTitle(getApplicationContext()));
                    }
                }
            }
        });

        EventViewModel eventViewModel =
                new ViewModelProvider(this).get(EventViewModel.class);


        eventViewModel.delete().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {

                        String data = content
                                .replace("[", "")
                                .replace("]", "")
                                .trim();

                        String[] parts = data.split(",");

                        long[] idxs = new long[parts.length];
                        for (int i = 0; i < parts.length; i++) {
                            idxs[i] = Long.parseLong(parts[i].trim());
                        }


                        String message;
                        if (idxs.length == 1) {
                            message = getString(R.string.delete_file);
                        } else {

                            message = String.valueOf(idxs.length)
                                    .concat(" ")
                                    .concat(getString(R.string.delete_files));
                        }
                        AtomicBoolean deleteThreads = new AtomicBoolean(true);
                        Snackbar snackbar = Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG);

                        snackbar.setAction(getString(R.string.revert_operation), (view) -> {

                            try {
                                deleteThreads.set(false);
                                FILES files = FILES.getInstance(getApplicationContext());
                                Executors.newSingleThreadExecutor().execute(() ->
                                        files.resetDeleting(idxs));
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            } finally {
                                snackbar.dismiss();
                            }

                        });

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (deleteThreads.get()) {
                                    ExecutorService executor = Executors.newSingleThreadExecutor();
                                    executor.execute(() -> {
                                        try {
                                            DOCS.getInstance(getApplicationContext())
                                                    .deleteDocuments(idxs);
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        }
                                    });

                                }
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }

                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.error().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.connections().observe(this, (event) -> {
            try {
                if (event != null) {
                    DOCS docs = DOCS.getInstance(getApplicationContext());
                    int connections = docs.getServer().numConnections();
                    LogUtils.error(TAG, "Number of server connections " + connections);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.reachability().observe(this, (event) -> {
            try {
                if (event != null) {
                    DOCS docs = DOCS.getInstance(getApplicationContext());
                    reachability.setText(docs.getNetworkReachability(getApplicationContext()));
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.fatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.warning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.content();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                liteViewModel.setShowFab(true);

                            }
                        });
                        liteViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.title().observe(this, event -> {
            try {
                if (event != null) {
                    searchBar.setText(InitApplication.getTitle(getApplicationContext()));
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            reachability.setText(docs.getNetworkReachability(getApplicationContext()));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        // show files view as primary fragment
        liteViewModel.setParentFileIdx(0);
    }


    private static class MainNetworkCallback extends ConnectivityManager.NetworkCallback {
        private final Context context;

        public MainNetworkCallback(Context context) {
            this.context = context;
        }

        @Override
        public void onAvailable(@NonNull Network network) {
            try {
                DOCS docs = DOCS.getInstance(context);

                if (!docs.getServer().hasPublicPeeraddrs()) {
                    docs.setReachability(DOCS.Reachability.LOCAL);
                } else {
                    docs.setReachability(DOCS.Reachability.RELAYS);
                    SwarmWorker.reservations(context,
                            ExistingWorkPolicy.REPLACE, 0);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        @Override
        public void onLost(@NonNull Network network) {
            try {
                DOCS.getInstance(context)
                        .setReachability(DOCS.Reachability.UNKNOWN);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

    }
}