package threads.odin.core.conns;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.odin.core.RoomConverters;

@Database(entities = {Conn.class}, version = 1, exportSchema = false)
@TypeConverters({RoomConverters.class})
public abstract class ConnsDatabase extends RoomDatabase {

    public abstract ConnDao connDao();

}
