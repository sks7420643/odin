package threads.odin.core.conns;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import tech.lp2p.core.Peeraddr;

public class CONNS {


    private static volatile CONNS INSTANCE = null;
    private final ConnsDatabase connsDatabase;

    private CONNS(ConnsDatabase connsDatabase) {
        this.connsDatabase = connsDatabase;
    }

    @NonNull
    private static CONNS createConns(@NonNull ConnsDatabase connsDatabase) {
        return new CONNS(connsDatabase);
    }

    public static CONNS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (CONNS.class) {
                if (INSTANCE == null) {
                    ConnsDatabase connsDatabase =
                            Room.inMemoryDatabaseBuilder(context,
                                            ConnsDatabase.class).
                                    allowMainThreadQueries().build();
                    INSTANCE = CONNS.createConns(connsDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public void addConn(@NonNull Peeraddr peeraddr) {
        Conn conn = new Conn(peeraddr);
        connsDatabase.connDao().insertConn(conn);
    }


    public void removeConn(@NonNull Peeraddr peeraddr) {
        connsDatabase.connDao().deleteConn(peeraddr);
    }

    @NonNull
    public ConnsDatabase database() {
        return connsDatabase;
    }
}
