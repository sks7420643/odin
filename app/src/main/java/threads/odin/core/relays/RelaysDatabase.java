package threads.odin.core.relays;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.odin.core.RoomConverters;

@Database(entities = {Relay.class}, version = 1, exportSchema = false)
@TypeConverters({RoomConverters.class})
public abstract class RelaysDatabase extends RoomDatabase {

    public abstract RelayDao relayDao();

}
