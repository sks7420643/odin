package threads.odin.core.relays;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import tech.lp2p.core.Peeraddr;

@Dao
public interface RelayDao {

    @Query("SELECT * FROM Relay")
    LiveData<List<Relay>> relays();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRelay(Relay relay);

    @Query("DELETE FROM Relay where peeraddr =:peeraddr")
    void deleteRelay(Peeraddr peeraddr);

}
