package threads.odin.core.relays;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import tech.lp2p.core.Reservation;

public class RELAYS {


    private static volatile RELAYS INSTANCE = null;
    private final RelaysDatabase relaysDatabase;

    private RELAYS(RelaysDatabase relaysDatabase) {
        this.relaysDatabase = relaysDatabase;
    }

    @NonNull
    private static RELAYS createConns(@NonNull RelaysDatabase relaysDatabase) {
        return new RELAYS(relaysDatabase);
    }

    public static RELAYS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (RELAYS.class) {
                if (INSTANCE == null) {
                    RelaysDatabase connsDatabase =
                            Room.inMemoryDatabaseBuilder(context,
                                            RelaysDatabase.class).
                                    allowMainThreadQueries().build();
                    INSTANCE = RELAYS.createConns(connsDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public void addRelay(@NonNull Reservation reservation) {
        Relay relay = new Relay(reservation.peeraddr());
        relaysDatabase.relayDao().insertRelay(relay);
    }


    public void removeRelay(@NonNull Reservation reservation) {
        relaysDatabase.relayDao().deleteRelay(reservation.peeraddr());
    }

    @NonNull
    public RelaysDatabase database() {
        return relaysDatabase;
    }
}
