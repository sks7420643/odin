package threads.odin.core.relays;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import tech.lp2p.core.Peeraddr;

@Entity
public record Relay(@PrimaryKey @NonNull @ColumnInfo(name = "peeraddr") Peeraddr peeraddr) {

    @Override
    @NonNull
    public Peeraddr peeraddr() {
        return peeraddr;
    }
}
