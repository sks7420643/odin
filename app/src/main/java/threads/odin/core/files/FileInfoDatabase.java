package threads.odin.core.files;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.odin.core.RoomConverters;


@Database(entities = {FileInfo.class}, version = 2, exportSchema = false)
@TypeConverters({RoomConverters.class})
public abstract class FileInfoDatabase extends RoomDatabase {

    public abstract FileInfoDao fileInfoDao();

}
