package threads.odin.core;


import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.OpenableColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.KeyPair;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Payloads;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.PrivateKey;
import tech.lp2p.core.PublicKey;
import tech.lp2p.core.Reservations;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.store.BLOCKS;
import tech.lp2p.store.PEERS;
import threads.odin.InitApplication;
import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.core.conns.CONNS;
import threads.odin.core.events.EVENTS;
import threads.odin.core.files.FILES;
import threads.odin.core.files.FileInfo;
import threads.odin.core.relays.RELAYS;
import threads.odin.utils.MimeTypeService;
import threads.odin.work.UploadFileWorker;
import threads.odin.work.UploadFilesWorker;

public class DOCS {
    public static final String IDX = "idx";
    public static final String URI = "uri";
    public static final String PNS = "pns";
    public static final String URL = "url";
    public static final String FILE = "file";

    public static final int CLICK_OFFSET = 500;
    public static final Payload PNS_RECORD = new Payload("pns-record", 20);
    private static final String TAG = DOCS.class.getSimpleName();
    private static final String IPFS_KEY = "IPFS_KEY";
    private static final String PRIVATE_KEY = "PRIVATE_KEY";
    private static final String PUBLIC_KEY = "PUBLIC_KEY";
    private static volatile DOCS INSTANCE = null;
    private final ReentrantLock lock = new ReentrantLock();
    private final FILES files;
    private final EVENTS events;
    private final String host;
    private final Session session;
    private final Server server;
    @NonNull
    private final AtomicReference<Dir> homepage = new AtomicReference<>();
    private final Lite lite;
    private Reachability reachability = Reachability.UNKNOWN;

    private DOCS(@NonNull Context context) throws Exception {
        Payloads payloads = new Payloads();
        payloads.put(PNS_RECORD.type(), PNS_RECORD);
        Lite.Settings settings = new Lite.Settings(
                keyPair(context), bootstrap(), "lite/1.0.0/", payloads);

        lite = new Lite(settings);
        files = FILES.getInstance(context);
        events = EVENTS.getInstance(context);
        BLOCKS blocks = BLOCKS.getInstance(context);
        PEERS peers = PEERS.getInstance(context);
        session = lite.createSession(blocks, peers);
        try {
            host = lite.self().toBase36();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

        CONNS conns = CONNS.getInstance(context);
        RELAYS relays = RELAYS.getInstance(context);
        server = lite.startServer(Lite.createServerSettings(5001),
                blocks,
                peers,
                conns::addConn,
                conns::removeConn,
                relays::addRelay,
                relays::removeRelay,
                peerId -> false,
                payload -> createEnvelope(context, payload),
                envelope -> {
                });

    }

    public static DOCS getInstance(@NonNull Context context) throws Exception {
        if (INSTANCE == null) {
            synchronized (DOCS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DOCS(context);
                }
            }
        }
        return INSTANCE;
    }

    private static String getNameWithoutExtension(@NonNull String file) {
        String fileName = new File(file).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? fileName : fileName.substring(0, dotIndex);
    }

    private static String getFileExtension(@NonNull String fullName) {
        String fileName = new File(fullName).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
    }

    public static String getCompactString(@NonNull String title) {
        return title.replace("\n", " ");
    }

    @NonNull
    public static String getSize(@NonNull FileInfo fileInfo) {

        String fileSize;
        long size = fileInfo.size();

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize + " B";
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize + " KB";
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize + " MB";
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean hasReadPermission(@NonNull Context context, @NonNull Uri uri) {
        int perm = context.checkUriPermission(uri, Binder.getCallingPid(), Binder.getCallingUid(),
                Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return perm != PackageManager.PERMISSION_DENIED;
    }

    @NonNull
    public static String getMimeType(@NonNull Context context, @NonNull Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        if (mimeType == null) {
            mimeType = MimeTypeService.OCTET_MIME_TYPE;
        }
        return mimeType;
    }

    @NonNull
    public static String getFileName(@NonNull Context context, @NonNull Uri uri) {
        String filename = null;

        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            filename = cursor.getString(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (filename == null) {
            filename = uri.getLastPathSegment();
        }

        if (filename == null) {
            filename = "file_name_not_detected";
        }

        return filename;
    }

    public static long getFileSize(@NonNull Context context, @NonNull Uri uri) {

        ContentResolver contentResolver = context.getContentResolver();

        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            return cursor.getLong(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        try (ParcelFileDescriptor fd = contentResolver.openFileDescriptor(uri, "r")) {
            Objects.requireNonNull(fd);
            return fd.getStatSize();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return -1;
    }

    public static boolean isPartial(@NonNull Context context, @NonNull Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri, new String[]{
                DocumentsContract.Document.COLUMN_FLAGS}, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();

            int docFlags = cursor.getInt(0);
            if ((docFlags & DocumentsContract.Document.FLAG_PARTIAL) != 0) {
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @NonNull
    public static File createTempFile(@NonNull Context context) throws IOException {
        return File.createTempFile("tmp", ".data", context.getCacheDir());
    }

    @NonNull
    public static File getTempFile(@NonNull Context context, @NonNull String filename) {
        return new File(context.getCacheDir(), filename);
    }

    @NonNull
    private static String getUniqueName(Set<String> names, @NonNull String name) {
        return getName(names, name, 0);
    }

    @NonNull
    private static String getName(Set<String> names, @NonNull String name, int index) {
        String searchName = name;
        if (index > 0) {
            try {
                String base = getNameWithoutExtension(name);
                String extension = getFileExtension(name);
                if (extension.isEmpty()) {
                    searchName = searchName.concat(" (" + index + ")");
                } else {
                    String end = " (" + index + ")";
                    if (base.endsWith(end)) {
                        String realBase = base.substring(0, base.length() - end.length());
                        searchName = realBase.concat(" (" + index + ")").concat(".").concat(extension);
                    } else {
                        searchName = base.concat(" (" + index + ")").concat(".").concat(extension);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
                searchName = searchName.concat(" (" + index + ")"); // just backup
            }
        }

        if (names.contains(searchName)) {
            return getName(names, name, ++index);
        }
        return searchName;
    }

    private static String checkMimeType(@Nullable String mimeType, @NonNull String name) {
        boolean evalDisplayName = false;
        if (mimeType == null) {
            evalDisplayName = true;
        } else {
            if (mimeType.isEmpty()) {
                evalDisplayName = true;
            } else {
                if (Objects.equals(mimeType, MimeTypeService.OCTET_MIME_TYPE)) {
                    evalDisplayName = true;
                }
            }
        }
        if (evalDisplayName) {
            mimeType = MimeTypeService.getMimeType(name);
        }
        return mimeType;
    }

    @NonNull
    private static Set<String> linkNames(@NonNull Session session, @NonNull Dir dir) throws Exception {
        Set<String> names = new HashSet<>();
        List<Info> childs = Lite.childs(session, dir);
        for (Info info : childs) {
            names.add(info.name());
        }
        return names;
    }

    public static boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return false;
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
        return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    @NonNull
    private static KeyPair keyPair(@NonNull Context context) throws Exception {

        if (!getPrivateKey(context).isEmpty() && !getPublicKey(context).isEmpty()) {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] publicKey = decoder.decode(getPublicKey(context));
            byte[] privateKey = decoder.decode(getPrivateKey(context));
            return new KeyPair(PublicKey.create(publicKey), new PrivateKey(privateKey));
        } else {
            KeyPair keys = Lite.generateKeyPair();
            Base64.Encoder encoder = Base64.getEncoder();
            setPrivateKey(context, encoder.encodeToString(keys.getPrivate().getEncoded()));
            setPublicKey(context, encoder.encodeToString(keys.getPublic().getEncoded()));
            return keys;
        }
    }

    public static void setPublicKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PUBLIC_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPublicKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PUBLIC_KEY, ""));
    }

    public static void setPrivateKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRIVATE_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPrivateKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PRIVATE_KEY, ""));

    }

    @NonNull
    public static Peeraddrs bootstrap() {
        return new Peeraddrs();
    }

    public static void file(@NonNull Context context, long parent, @NonNull Uri uri) {

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);

        Executors.newSingleThreadExecutor().submit(() -> {
            try {
                DOCS docs = DOCS.getInstance(context);
                String name = DOCS.getFileName(context, uri);
                String mimeType = DOCS.getMimeType(context, uri);
                long size = DOCS.getFileSize(context, uri);

                long idx = docs.createDocument(parent, mimeType, null,
                        uri, name, size);

                UploadFileWorker.load(context, idx);


            } catch (Throwable throwable) {
                EVENTS.getInstance(context).error(
                        context.getString(R.string.file_not_found));
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }

    public static void files(@NonNull Context context, @NonNull ClipData data, long parent) {

        try {

            int items = data.getItemCount();

            if (items > 0) {

                File file = DOCS.createTempFile(context);

                try (PrintStream out = new PrintStream(file)) {
                    for (int i = 0; i < items; i++) {
                        ClipData.Item item = data.getItemAt(i);
                        Uri uri = item.getUri();
                        out.println(uri.toString());
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                UploadFilesWorker.load(context, parent, file);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private Envelope createEnvelope(Context context, Payload payload) {
        try {
            if (Objects.equals(PNS_RECORD, payload)) {
                Dir page = getHomePage(context);
                return Lite.createEnvelope(server, PNS_RECORD, page.cid());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    @NonNull
    public Reservations reservations() {
        return Lite.reservations(server);
    }

    public long createDirectory(long parent, @NonNull String name) throws Exception {
        lock.lock();
        try {
            Dir directory = Lite.createEmptyDirectory(session, name);
            long idx = createDocument(parent, MimeTypeService.DIR_MIME_TYPE,
                    directory.cid(), null, name, directory.size());
            finishDocument(idx);
            return idx;
        } finally {
            lock.unlock();
        }
    }

    public void createTextFile(long parent, @NonNull String text) {
        lock.lock();

        String timeStamp = DateFormat.getDateTimeInstance().
                format(new Date()).
                replace(":", "").
                replace(".", "_").
                replace("/", "_").
                replace(" ", "_");

        String name = "TXT_" + timeStamp + ".txt";

        try (InputStream inputStream = new ByteArrayInputStream(text.getBytes())) {
            Fid fid = Lite.storeInputStream(session, name, inputStream);
            long idx = createDocument(parent, MimeTypeService.PLAIN_MIME_TYPE, fid.cid(),
                    null, name, text.length());

            finishDocument(idx);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    public Uri getHomePageUri() {
        return Uri.parse(DOCS.PNS + "://" + host);
    }

    public void deleteDocuments(long... idxs) {
        lock.lock();
        try {
            removeFromParentDocument(idxs);
            deleteRecursiveDocuments(idxs);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    private void removeFromParentDocument(long... idxs) {
        for (long idx : idxs) {
            try {
                removeFromParentDocument(idx);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }

    private void deleteRecursiveDocuments(long... idxs) {
        try {
            for (long idx : idxs) {
                List<FileInfo> children = files.getChildren(idx);
                for (FileInfo fileInfo : children) {
                    deleteRecursiveDocuments(fileInfo.idx());
                }
            }
            files.finalDeleting(idxs);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private Set<String> linkNames(@NonNull Session session, long parent) throws Exception {
        Dir dirCid;
        if (parent > 0) {
            // child is located in a directory
            Cid dir = files.getContent(parent);
            Objects.requireNonNull(dir);
            dirCid = (Dir) Lite.info(session, dir);
        } else {
            // child is top level file (normally nothing to do, but we will update the page content)
            dirCid = homepage.get();
        }
        Objects.requireNonNull(dirCid);
        return linkNames(session, dirCid);

    }

    public void finishDocument(long idx) throws Exception {
        lock.lock();
        try {
            updateParent(idx);
        } finally {
            lock.unlock();
        }
    }

    private Dir updateDirectory(Dir dir, FileInfo fileInfo) throws Exception {
        Info info = Lite.info(session, Objects.requireNonNull(fileInfo.cid()));
        if (info instanceof Dir child) {
            return Lite.updateDirectory(session, dir, child);
        }
        if (info instanceof Fid child) {
            return Lite.updateDirectory(session, dir, child);
        }
        throw new IllegalStateException();
    }

    private void updateParent(long idx) throws Exception {

        FileInfo child = files.getFileInfo(idx);
        Objects.requireNonNull(child);

        long parent = child.parent();

        Cid cid = child.cid();
        Objects.requireNonNull(cid);

        if (parent > 0) {
            // child is located in a directory
            Cid dirCid = Objects.requireNonNull(files.getContent(parent));
            Objects.requireNonNull(dirCid);

            Info info = Lite.info(session, dirCid); // todo this is expensive (maybe store info in DB)
            Dir dir = (Dir) info;
            Objects.requireNonNull(dir);

            Dir directory = updateDirectory(dir, child);
            Objects.requireNonNull(directory);

            files.updateContent(parent, directory.cid(), directory.size(),
                    System.currentTimeMillis());
            updateParent(parent);
        } else {
            // child is top level file (normally nothing to do, but we will update the page content)
            Dir dirCid = homepage.get();
            Objects.requireNonNull(dirCid);

            Dir directory = updateDirectory(dirCid, child);
            Objects.requireNonNull(directory);

            homepage.set(directory); // update homepage
        }
    }

    private Dir removeFromDirectory(Dir dir, FileInfo fileInfo) throws Exception {
        Info info = Lite.info(session, Objects.requireNonNull(fileInfo.cid()));
        if (info instanceof Dir child) {
            return Lite.removeFromDirectory(session, dir, child);
        }
        if (info instanceof Fid child) {
            return Lite.removeFromDirectory(session, dir, child);
        }
        throw new IllegalStateException();
    }

    private void removeFromParentDocument(long idx) throws Exception {
        FileInfo child = files.getFileInfo(idx);
        if (child != null) {
            long parent = child.parent();
            if (parent > 0) {
                Cid dirCid = Objects.requireNonNull(files.getContent(parent));
                Objects.requireNonNull(dirCid);
                Info info = Lite.info(session, dirCid);
                Dir dir = (Dir) info;
                Objects.requireNonNull(dir);
                Dir newDir = removeFromDirectory(dir, child);
                files.updateContent(parent, newDir.cid(),
                        newDir.size(), System.currentTimeMillis());
                updateParent(parent);
            } else {
                Dir dir = homepage.get();
                Objects.requireNonNull(dir);
                Dir directory = removeFromDirectory(dir, child);
                homepage.set(directory); // update homepage
            }
        }
    }

    public long createDocument(long parent, @Nullable String type,
                               @Nullable Cid cid, @Nullable Uri uri, @NonNull String displayName,
                               long size) throws Exception {
        Set<String> names = linkNames(session, parent);
        String name = getUniqueName(names, displayName);
        String mimeType = checkMimeType(type, displayName);
        FileInfo fileInfo = FILES.createFileInfo(name, parent, mimeType, cid, size, uri);
        return files.storeFileInfo(fileInfo);
    }

    public void initPinsPage(@NonNull Context context) {
        lock.lock();

        try {
            String title = InitApplication.getTitle(context);
            List<FileInfo> pins = files.getPins();
            List<Info> fileLinks = new ArrayList<>();
            boolean isEmpty = pins.isEmpty();
            if (!isEmpty) {
                for (FileInfo pin : pins) {
                    Cid link = pin.cid();
                    Objects.requireNonNull(link);
                    fileLinks.add(Lite.info(session, link));
                }
            }
            Dir dir = Lite.createDirectory(session, title, fileLinks);
            Objects.requireNonNull(dir);
            homepage.set(dir);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    public Uri pnsUri(@NonNull FileInfo fileInfo) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(DOCS.PNS).authority(host);
        List<FileInfo> ancestors = files.getAncestors(fileInfo.idx());
        for (FileInfo ancestor : ancestors) {
            builder.appendPath(ancestor.name());
        }
        return builder.build();
    }

    @NonNull
    public Dir getHomePage(@NonNull Context context) {
        Dir page = homepage.get();
        try {
            if (page == null) {
                String title = InitApplication.getTitle(context);
                page = Lite.createEmptyDirectory(session, title);
                Objects.requireNonNull(page);
                homepage.set(page);
            }
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
        return page;
    }

    @NonNull
    public Server getServer() {
        return server;
    }

    public void setReachability(Reachability reachability) {
        if (reachability != this.reachability) {
            this.reachability = reachability;
            events.reachability(reachability.name());
        }
    }

    public String getNetworkReachability(@NonNull Context context) {
        return switch (reachability) {
            case UNKNOWN -> context.getString(R.string.unknown);
            case LOCAL -> context.getString(R.string.local_network);
            case RELAYS -> context.getString(R.string.relays_network);
        };
    }

    @NonNull
    public Lite lite() {
        return lite;
    }

    public enum Reachability {
        UNKNOWN, LOCAL, RELAYS
    }
}
